(ns status-check.incidents.core
  "Incidents represent the history of downtime of each url.

  - An incident is created on any url request failure.
  - The incident is closed on the url request success.
  - Incident creation alerts are sent according to escalation points.
  - Escalation points are defined as alerters' attributes
  - Incident closures are being alerted immediately.
  - On incident closure only escalated alerters are being triggered.
  "

  (:require [clojure.tools.logging :as log])
  (:require [korma.core :refer [select where values set-fields] :as k]
            [korma.db :refer [transaction]])
  (:require [status-check.db.models :as models]
            [status-check.db.utils :refer [kinsert] :as dbutils]))

;; TODO Unittest

(defn update-all! [url-id]
  (transaction
   (let [n-rows-updated
         (k/update models/incidents
                   (where {:is-closed false
                           :url-id url-id})
                   (set-fields {:updated-at (java.util.Date.)}))]
     (when (= n-rows-updated 0)
       (kinsert models/incidents (values {:url-id url-id}))))))

(defn close-all! [url-id]
  (k/update models/incidents
            (where {:is-closed false
                    :url-id url-id})
            (set-fields {:is-closed true
                         :updated-at (java.util.Date.)
                         :closed-at (java.util.Date.)})))

(defn update-escalated-at! [incident-id date]
  (k/update models/incidents
            (where {:id incident-id})
            (set-fields {:escalated-at date})))
