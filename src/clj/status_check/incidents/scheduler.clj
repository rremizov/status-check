(ns status-check.incidents.scheduler
  (:require [clojure.tools.logging :as log])
  (:require [clojurewerkz.quartzite.conversion :as qc]
            [clojurewerkz.quartzite.jobs :as j]
            [clojurewerkz.quartzite.scheduler :as qs]
            [clojurewerkz.quartzite.triggers :as t]
            [mount.core :refer [defstate]])
  (:require [status-check.db.core :as db]
            [status-check.scheduler.core :refer [scheduler]]
            [status-check.scheduler.jobs :refer [build-job]]
            [status-check.scheduler.triggers :as triggers]))

(j/defjob ^:private IncidentsCleanupJob [ctx]
  (db/incidents-cleanup!))

(def job-key "incidents-cleanup")

(defn- build-cleanup-trigger []
  (triggers/build-periodic-trigger job-key 60))

(defn- build-cleanup-job []
  (build-job job-key IncidentsCleanupJob {}))

(defn- start! []
  (qs/schedule scheduler (build-cleanup-job) (build-cleanup-trigger)))

(defstate schedule
  :start (start!)
  :stop :stopped)
