(ns status-check.checks.core
  (:require [clojure.tools.logging :as log])
  (:require [korma.core :refer [select fields where set-fields values] :as k]
            [korma.db :refer [transaction]])
  (:require [status-check.db.core :as db]
            [status-check.db.models :as models]
            [status-check.db.utils :refer [kinsert] :as dbutils]
            [status-check.checks.scheduler :as checks-scheduler]
            [status-check.utils :as utils]))

(defn owner? [user-id check-id]
  (= user-id
     (-> (select models/checks
                 (fields :owner-id)
                 (where {:id check-id}))
         first
         :owner-id)))

(defn update! [id active? title check-interval-in-seconds http-method url]
  (transaction
   (if-let [check-url-id
            (-> (select models/checks
                        (fields :url-id)
                        (where {:id id}))
                first
                :url-id)]
     (k/update models/urls
               (set-fields {:http-method http-method :url url})
               (where {:id check-url-id}))
     (k/update models/checks
               (set-fields {:url-id
                            (:id (kinsert models/urls
                                          (values {:http-method http-method
                                                   :url url})))})
               (where {:id id})))
   (k/update models/checks
             (set-fields {:title title
                          :check-interval (dbutils/map->pginterval
                                           {:seconds check-interval-in-seconds})
                          :is-active active?})
             (where {:id id}))
   (let [check (models/check-by-id id)]
     (checks-scheduler/unschedule! check)
     (when (:is-active check)
       (checks-scheduler/schedule! check)))))

(defn create! [owner-id title]
  (kinsert models/checks (values {:title title :owner-id owner-id})))
