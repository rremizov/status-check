(ns status-check.checks.scheduler
  (:require [clojure.tools.logging :as log])
  (:require [clj-http.client :as http-client]
            [clojurewerkz.quartzite.conversion :as qc]
            [clojurewerkz.quartzite.jobs :as j]
            [clojurewerkz.quartzite.scheduler :as qs]
            [clojurewerkz.quartzite.triggers :as t]
            [korma.core :refer [select where with values]]
            [mount.core :refer [defstate]])
  (:require [status-check.alerters.core :as alerters]
            [status-check.urls.core :as urls]
            [status-check.db.models :as models]
            [status-check.db.utils :refer [kinsert]]
            [status-check.scheduler.core :refer [scheduler]]
            [status-check.scheduler.jobs :refer [build-job]]
            [status-check.scheduler.triggers :as triggers]
            [status-check.utils :as utils]))

(j/defjob ^:private checkURLJob [ctx]
  (let [check-id (get (qc/from-job-data ctx) "check-id")
        check (models/check-by-id check-id)
        url-id (:url-id check)
        url
        (first (select models/urls
                       (with models/url-parameters)
                       (where {:id url-id})))
        previous-response (urls/last-response url-id)
        new-response (urls/make-request! url)]
    (when (and (not (nil? previous-response))
               (not= (:status new-response) (:status previous-response)))
      (kinsert models/checks-statuses-changes
               (values {:check-id check-id
                        :from-id (:id previous-response)
                        :to-id (:id new-response)})))))

(defn- make-key [check]
  (keyword (str "trigger-check-" (:id check))))

(defn- build-check-trigger [check]
  (log/debug "build-check-trigger" check)
  (triggers/build-periodic-trigger
   (make-key check)
   (utils/interval->seconds (:check-interval check))))

(defn- build-check-job [check]
  (log/debug "build-check-job" check)
  (build-job (make-key check) checkURLJob {:check-id (:id check)}))

(defn unschedule! [check]
  (log/debug "unschedule!" check)
  (qs/delete-trigger scheduler (t/key (make-key check))))

(defn schedule! [check]
  (log/debug "schedule!" check)
  (qs/schedule scheduler
               (build-check-job check)
               (build-check-trigger check)))

(defn- start! []
  (log/debug "start!")
  (doseq [check (select (models/active-checks))]
    (schedule! check)))

(defstate schedule
  :start (start!)
  :stop :stopped)
