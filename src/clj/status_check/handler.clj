(ns status-check.handler
  (:require [clojure.tools.logging :as log])
  (:require [compojure.core :refer [routes wrap-routes]]
            [compojure.route :as route]
            [luminus.logger :as logger]
            [mount.core :as mount])
  ;; Entry point must require all namespaces with `defstate`
  (:require [status-check.urls.scheduler]
            [status-check.config :refer [env]]
            [status-check.db.core]
            [status-check.escalation.scheduler]
            [status-check.incidents.scheduler]
            [status-check.checks.scheduler]
            [status-check.scheduler.core :refer [scheduler]])
  (:require [status-check.env :refer [defaults]]
            [status-check.layout :refer [error-page]]
            [status-check.middleware :as middleware]
            [status-check.routes.alerters :refer [alerters-routes]]
            [status-check.routes.urls :refer [urls-routes]]
            [status-check.routes.auth :refer [auth-routes]]
            [status-check.routes.core :refer [core-routes]]
            [status-check.routes.incidents :refer [incidents-routes]]
            [status-check.routes.checks :refer [checks-routes]]
            [status-check.routes.share :refer [share-routes]]
            [status-check.routes.summary :refer [summary-routes]]))

(mount/defstate init-app
  :start ((or (:init defaults) identity))
  :stop  ((or (:stop defaults) identity)))

(mount/defstate log :start (logger/init (:log-config env)))

(defn init
  "init will be called once when
  app is deployed as a servlet on
  an app server such as Tomcat
  put any initialization code here"
  []
  (doseq [component (:started (mount/start))]
    (log/info component "started")))

(defn destroy
  "destroy will be called when your application
  shuts down, py any clean up code here"
  []
  (doseq [component (:stopped (mount/stop))]
    (log/info component "stopped"))
  (shutdown-agents)
  (log/info "status-check has shut down!"))

(def app-routes
  (routes
   (var core-routes)
   (var auth-routes)
   (var summary-routes)
   (var incidents-routes)
   (var checks-routes)
   (var alerters-routes)
   (var urls-routes)
   (var share-routes)
   (route/not-found
    (:body
     (error-page {:status 404
                  :title "page not found"})))))

(mount/defstate app
  :start (middleware/wrap-base #'app-routes))
