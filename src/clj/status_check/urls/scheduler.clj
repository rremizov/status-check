(ns status-check.urls.scheduler
  (:require [clojure.tools.logging :as log])
  (:require [clojurewerkz.quartzite.jobs :as j]
            [clojurewerkz.quartzite.scheduler :as qs]
            [mount.core :refer [defstate]])
  (:require [status-check.db.core :as db]
            [status-check.scheduler.core :refer [scheduler]]
            [status-check.scheduler.jobs :refer [build-job]]
            [status-check.scheduler.triggers :as triggers]))

(j/defjob ^:private URLResponsesCleanupJob [ctx]
  (db/url-responses-cleanup!))

(def job-key "url-responses-cleanup")

(defn- build-cleanup-trigger []
  (log/debug "status-check.scheduler.url-responses/build-trigger")
  (triggers/build-periodic-trigger job-key 60))

(defn- build-cleanup-job []
  (log/debug "status-check.scheduler.url-responses/build-job")
  (build-job job-key URLResponsesCleanupJob {}))

(defn- start! []
  (log/debug "status-check.scheduler.url-responses/schedule-cleanup!")
  (qs/schedule scheduler (build-cleanup-job) (build-cleanup-trigger)))

(defstate schedule
  :start (start!)
  :stop :stopped)
