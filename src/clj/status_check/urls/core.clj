(ns status-check.urls.core
  (:require [clojure.set :refer [rename-keys]]
            [clojure.string :as string]
            [clojure.tools.logging :as log])
  (:require [cheshire.core :as json]
            [clj-http.client :as http-client]
            [korma.core :refer [select fields where set-fields values] :as k]
            [korma.db :refer [transaction]])
  (:require [status-check.urls.scheduler]
            [status-check.db.core :as db]
            [status-check.db.models :as models]
            [status-check.db.utils :refer [kinsert]]
            [status-check.incidents.core :as incidents]))

(def okay-response-codes #{200})

(defn owner? [user-id url-id]
  (->> [(models/check-by-url-id url-id)
        (models/alerter-by-url-id url-id)]
       (map :owner-id)
       (some #{user-id})
       nil?
       not))

(defn update-parameter! [id name value]
  (k/update models/url-parameters
            (set-fields {:name name :value value})
            (where {:id id})))

(defn create-parameter! [url-id name value]
  (kinsert models/url-parameters
           (values {:url-id url-id
                    :name name
                    :value value})))

(defn responses [url-id]
  ;; TODO Unittest
  (-> (k/select* models/url-responses)
      (where {:url-id url-id})
      (k/order :id :DESC)
      (k/limit 10)
      select))

(defn response [url-id response-id]
  ;; TODO Unittest
  (-> (k/select* models/url-responses)
      (where {:url-id url-id
              :id response-id})
      select
      first))

(defmulti pretty-print-response-body #(get-in % [:headers :Content-Type]))

(defmethod pretty-print-response-body :default [response] (:body response))

(defmethod pretty-print-response-body "application/json"
  [response]
  (json/generate-string (json/parse-string (:body response)) {:pretty true}))

(defn pretty-print-response [response]
  ;; TODO Unittest
  (assoc response :pretty-printed-body (pretty-print-response-body response)))

(defn pretty-print-responses [responses]
  ;; TODO Unittest
  (map pretty-print-response responses))

(defn last-response [url-id]
  (-> (k/select* models/url-responses)
      (where {:url-id url-id})
      (k/order :id :DESC)
      select
      first))

(defn- prepare-response [url response]
  (merge {:url-id (:id url)}
         (rename-keys (select-keys response [:status :headers :body :request-time])
                      {:request-time :request-time})))

(defn save-response! [[url response]]
  (let [response
        (db/save-url-response<! (prepare-response url response))]
    (if (contains? okay-response-codes (:status response))
      (incidents/close-all! (response :url-id))
      (incidents/update-all! (response :url-id)))
    response))

(defn- prepare-request-params [url]
  {(if (= (:http-method url) "GET")
     :query-params
     :form-params)
   (reduce (fn [acc x] (assoc acc (:name x) (:value x))) {}
           (:url-parameters url))})

(def ^:private prepare-http-method-name
  (comp keyword string/lower-case :http-method))

(defn- prepare-request [url]
  {:pre [(not (nil? url))]}
  (merge {:throw-exceptions false
          :url (:url url)
          :method (prepare-http-method-name url)
          :conn-timeout 30000
          :socket-timeout 30000}
         (prepare-request-params url)))

(defn- request [url]
  [url
   (try (http-client/request (prepare-request url))
        (catch java.net.SocketTimeoutException exc
          {:status nil :request-time nil :headers {}
           :body "socket timeout"})
        (catch java.net.ConnectException exc
          {:status nil :request-time nil :headers {}
           :body "connection refused"})
        (catch java.net.UnknownHostException exc
          {:status nil :request-time nil :headers {}
           :body "unknown host"}))])

(def make-request! (comp save-response! request))
