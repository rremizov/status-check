(ns status-check.alerters.validators
  (:require [bouncer.core :as b]
            [bouncer.validators :as v]
            [buddy.hashers :as hashers]
            [korma.core :refer [select fields]])
  (:require [status-check.alerters.core :as alerters]
            [status-check.db.models :as models]
            [status-check.utils :refer [parse-int]]))

(v/defvalidator alerter-type?
  {:default-message-format "\"%s\" is not an alerter type"}
  [p]
  (contains? (->> (select models/alerter-types (fields :name))
                  (map :name)
                  (set))
             p))

(v/defvalidator owner?
  {:default-message-format "not authorized"}
  [alerter-id user-id]
  (alerters/owner? (parse-int user-id) (parse-int alerter-id)))
