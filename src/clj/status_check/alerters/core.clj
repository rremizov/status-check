(ns status-check.alerters.core
  (:require [clojure.tools.logging :as log])
  (:require [korma.core :refer [select fields where set-fields values] :as k]
            [korma.db :refer [transaction]])
  (:require [status-check.alerters.types :refer [alert!]]
            [status-check.db.models :as models]
            [status-check.db.utils :refer [kinsert]]
            [status-check.utils :as utils]))

(defn owner? [user-id alerter-id]
  (= user-id
     (-> (select models/alerters (fields :owner-id) (where {:id alerter-id}))
         first
         :owner-id)))

(defn create! [owner-id title]
  (kinsert models/alerters (values {:title title :owner-id owner-id})))

(defn send-alert! [{:keys [incident-url incident-id alerter-id]}]
  ;; TODO Unittests
  (log/debug "SEND-ALERT!" incident-url incident-id alerter-id)
  (when-let [alerter (first (select models/active-alerters
                                    (where {:id alerter-id :is-active true})))]
    (alert! alerter
            (first (select models/incidents (where {:id incident-id})))
            incident-url)))

(defn update! [id active? title alerter-type-name
               http-method url
               escalation-point-seconds]
  (transaction
   (if-let [url-id
            (-> (select models/alerters
                        (fields :url-id)
                        (where {:id id}))
                first
                :url-id)]
     (k/update models/urls
               (set-fields {:http-method http-method :url url})
               (where {:id url-id}))
     (k/update models/alerters
               (set-fields {:url-id
                            (:id (kinsert models/urls
                                          (values {:http-method http-method
                                                   :url url})))})
               (where {:id id})))
   (k/update models/alerters
             (set-fields {:title title
                          :type-id
                          (:id (models/alerter-type-by-name alerter-type-name))
                          :escalation-point-seconds escalation-point-seconds
                          :is-active active?})
             (where {:id id}))))
