(ns status-check.alerters.types
  (:require [clojure.string :as str]
            [clojure.tools.logging :as log])
  (:require [korma.core :refer [select where values] :as k])
  (:require [status-check.urls.core :as urls]
            [status-check.config :refer [env]]
            [status-check.db.models :as models]
            [status-check.db.utils :refer [kinsert ->pgjsonb]]
            [status-check.utils :as utils]))

;; TODO Unittest

(def type-name-slack "Slack")

(defmulti alert!
  (fn [alerter & more]
    (log/info "ALERT!" alerter more)
    (-> alerter :type-name str/lower-case keyword)))

(defmethod alert! :default [& more])

(defmethod alert! :slack
  [alerter incident incident-url]
  (let [url (models/url-by-id (:url-id alerter))
        url-parameters
        (models/url-parameters-by-id (:url-id alerter))
        channel (->> url-parameters
                     (filter #(= (:name %) "channel"))
                     first
                     :value)

        fallback-context [(:id incident)
                          incident-url
                          (if (:is-closed incident) "Closed" "Opened")
                          (utils/date->str (:created-at incident))
                          (utils/format-time-interval
                           (:created-at incident) (:updated-at incident))]
        fallback-template
        (str "Incident#%d\n"
             "URL: %s\n"
             "Status: %s\n"
             "Created at: %s\n"
             "Duration: %s\n")
        fallback-message (apply format fallback-template fallback-context)]
    (kinsert models/alerter-alert
             (values {:alerter-id (:id alerter)
                      :payload (->pgjsonb {:channel channel :message fallback-message})}))
    (let [slack-response
          (utils/message->slack
           (:url url)
           nil
           channel
           [{:fallback fallback-message
             :color (if (:is-closed incident) "good" "danger")
             :title (str "Incident#" (:id incident))
             ;; :title_link "" ; TODO URL of the incident
             :fields [{:title "URL"
                       :value incident-url}
                      {:title "Created at"
                       :value (utils/date->str (:created-at incident))}
                      {:title "Status"
                       :value (if (:is-closed incident) "Closed" "Opened")
                       :short true}
                      {:title "Duration"
                       :value (utils/format-time-interval
                               (:created-at incident) (:updated-at incident))
                       :short true}]}])]
      (urls/save-response! [url slack-response]))))
