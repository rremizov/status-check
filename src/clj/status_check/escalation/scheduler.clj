(ns status-check.escalation.scheduler
  (:require [clojure.tools.logging :as log])
  (:require [clojurewerkz.quartzite.conversion :as qc]
            [clojurewerkz.quartzite.jobs :as j]
            [clojurewerkz.quartzite.scheduler :as qs]
            [clojurewerkz.quartzite.triggers :as t]
            [mount.core :refer [defstate]])
  (:require [status-check.db.core :as db]
            [status-check.escalation.core :as escalation]
            [status-check.scheduler.core :refer [scheduler]]
            [status-check.scheduler.jobs :refer [build-job]]
            [status-check.scheduler.triggers :as triggers]))

(j/defjob ^:private IncidentsJob [ctx]
  (escalation/escalate!))

(def incidents-job-key "escalate-incidents")

(defn- build-incidents-trigger []
  (triggers/build-periodic-trigger incidents-job-key 10))

(defn- build-incidents-job []
  (build-job incidents-job-key IncidentsJob {}))

(defn- start! []
  (qs/schedule scheduler (build-incidents-job) (build-incidents-trigger)))

(defstate schedule
  :start (start!)
  :stop :stopped)
