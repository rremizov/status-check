(ns status-check.escalation.core
  (:require [clojure.tools.logging :as log])
  (:require [korma.core :refer [select where] :as k]
            [korma.db :refer [transaction]])
  (:require [status-check.alerters.core :as alerters]
            [status-check.db.core :as db]
            [status-check.incidents.core :as incidents]
            [status-check.db.models :as models]
            [status-check.db.utils :refer [kinsert] :as dbutils]))

;; TODO Unittest

(defn escalate! []
  (doseq [escalation (concat (db/incidents-pending-escalations)
                             (db/incidents-pending-closure-alerts))]
    (let [now (java.util.Date.)]
      (alerters/send-alert! escalation)
      (incidents/update-escalated-at! (:incident-id escalation) now))))
