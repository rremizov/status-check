(ns status-check.utils
  (:require [cheshire.core :as json]
            [clj-http.client :as http-client]
            [clj-time.coerce :as time-coerce]
            [clj-time.core :as time]
            [clj-time.format :as time-format]))

(def http-methods
  #{"HEAD"
    "OPTIONS"
    "GET"
    "POST"
    "PUT"
    "PATCH"
    "DELETE"})

(defn date->str [date]
  (time-format/unparse (time-format/formatters :rfc822)
                       (time-coerce/from-date date)))

(defn format-time-interval [date0 date1]
  (let [interval (time/interval (time-coerce/from-date date0)
                                (time-coerce/from-date date1))
        in-hours (time/in-hours interval)
        in-minutes (time/in-minutes interval)]
    (if (> in-hours 0)
      (str in-hours " hour(s)")
      (str in-minutes " minute(s)"))))

(defprotocol IParseInt (parse-int [this]))

(extend-protocol IParseInt
  nil (parse-int [this] this)
  java.lang.Integer (parse-int [this] this)
  java.lang.String
  (parse-int [this] (some->> this (re-find #"\d+") Integer/parseInt)))

(defprotocol Roundable
  (round [this] "Round value"))

(extend-protocol Roundable
  nil
  (round [this] 0)
  java.lang.Double
  (round [this] (Math/round this))
  java.math.BigDecimal
  (round [this] (.setScale this 0 java.math.RoundingMode/HALF_UP)))

(defrecord TimeInterval [seconds minutes hours days months years])

(defn interval->seconds [v]
  ;; Assuming every month has 30 days.
  (int (+ (:seconds v)
          (* 60 (:minutes v))
          (* 60 60 (:hours v))
          (* 24 60 60 (:days v))
          (* 30 24 60 60 (:months v))
          (* 360 30 24 60 60 (:years v)))))

(defn message->slack
  ([url message channel] (message->slack url message channel []))
  ([url message channel attachments]
   (let [body  {:text message
                :channel channel
                :attachments attachments}]
     (try (http-client/request
           {:throw-exceptions false
            :conn-timeout 30000
            :socket-timeout 30000
            :url url
            :method :post
            :headers {"Content-type" "application/json"}
            :body (json/generate-string body)})
          (catch java.net.SocketTimeoutException exc
            {:status nil :request-time nil :headers {}
             :body "socket timeout"})
          (catch java.net.ConnectException exc
            {:status nil :request-time nil :headers {}
             :body "connection refused"})
          (catch java.net.UnknownHostException exc
            {:status nil :request-time nil :headers {}
             :body "unknown host"})))))
