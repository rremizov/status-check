(ns status-check.sharing.core
  (:require [korma.core :refer [select where values delete]]
            [korma.db :refer [transaction]])
  (:require [status-check.db.models :as models]
            [status-check.db.utils :refer [kinsert]])
  (:import [java.util UUID]))

(defn make-access-token [] (UUID/randomUUID))

(defn uri->href [uri]
  (str (:uri uri) "?"
       "user-id=" (:owner-id uri)
       "&access-token="
       (java.net.URLEncoder/encode (str (:access-token uri)) "UTF-8")))

(defn shared-uris->strings
  ([owner-id]
   (->> (select models/shared-uris (where {:owner-id owner-id}))
        (map #(assoc % :href (uri->href %)))))
  ([owner-id uri]
   (->> (select models/shared-uris (where {:owner-id owner-id :uri uri}))
        (map #(assoc % :href (uri->href %)))))
  ([owner-id uri access-token]
   (->> (select models/shared-uris
                (where {:owner-id owner-id
                        :uri uri
                        :access-token
                        (UUID/fromString access-token)}))
        (map #(assoc % :href (uri->href %))))))

(defn share-uri! [owner-id uri]
  (kinsert models/shared-uris
           (values {:owner-id owner-id
                    :uri uri
                    :access-token (make-access-token)})))

(defn delete-shared-uri! [uri-id]
  (delete models/shared-uris (where {:id uri-id})))

(defn owner? [user-id uri-id]
  (->> (where {:owner-id user-id :id uri-id})
       (select models/shared-uris)
       first
       nil?
       not))

(defn authorized? [user-id uri access-token]
  (->> (where {:owner-id user-id
               :access-token (UUID/fromString access-token)
               :uri uri})
       (select models/shared-uris)
       first
       nil?
       not))
