(ns status-check.validators
  (:require [bouncer.core :as b]
            [bouncer.validators :as v])
  (:require [status-check.utils :as utils]))

(v/defvalidator http-method?
  {:default-message-format "%s is not a http method"}
  [p]
  (utils/http-methods p))

(v/defvalidator contains-integer?
  {:default-message-format "%s is not an integer"}
  [p]
  (not (nil? (utils/parse-int p))))

(v/defvalidator positive-integer?
  {:default-message-format "%s is not a positive integer"}
  [p]
  (> (utils/parse-int p) 0))

(v/defvalidator non-negative-integer?
  {:default-message-format "%s is not a non-negative integer"}
  [p]
  (>= (utils/parse-int p) 0))
