(ns status-check.filters)

(defn boolean->glyph [v]
  (let [glyphs {true "<span class=\"fa fa-check\" />"
                false "<span class=\"fa fa-remove\" />"}]
    [:safe (glyphs (boolean v))]))
