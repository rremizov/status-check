(ns status-check.routes.incidents
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.string :as str]
            [clojure.tools.logging :as log])
  (:require [bouncer.core :as b]
            [bouncer.validators :as v]
            [buddy.auth :refer [authenticated?]]
            [buddy.auth.accessrules :refer [wrap-access-rules]]
            [compojure.core :refer [defroutes context GET POST]]
            [ring.util.response :refer [redirect]]
            [korma.core :refer [values select fields where set-fields]])
  (:require [status-check.alerters.validators :as alerters-validators]
            [status-check.urls.core :refer [okay-response-codes]]
            [status-check.db.core :as db]
            [status-check.db.models :as models]
            [status-check.layout :as layout :refer [*identity* make-redirect]]
            [status-check.middleware :as middleware]
            [status-check.checks.core :as checks]
            [status-check.sharing.core :as sharing]
            [status-check.utils :refer [parse-int]]
            [status-check.validators :as validators]))

(defn page-incidents [{:keys [uri params]}]
  (let [user-id (or *identity* (parse-int (:user-id params)))
        user (models/user-by-id user-id)
        shared-uris (sharing/shared-uris->strings user-id uri)
        current-shared-uri
        (when (nil? *identity*)
          (first (sharing/shared-uris->strings user-id uri (:access-token params))))
        incidents-summary
        (select models/incidents-summary
                (where (if-let [filter-url (:filter-url params)]
                         {:owner-id user-id :url filter-url}
                         {:owner-id user-id})))]
    (layout/render "incidents/index.html" {:user user
                                           :shared-uris shared-uris
                                           :current-shared-uri current-shared-uri
                                           :incidents-summary incidents-summary})))

(defn owner? [{:keys [match-params]}]
  (checks/owner? *identity* (-> match-params :id parse-int)))

(defn- can-view? [{:keys [params uri]}]
  ;; TODO Unittest
  (and (:user-id params)
       (:access-token params)
       (sharing/authorized? (parse-int (:user-id params))
                            uri
                            (:access-token params))))

(def access-rules
  [{:uris ["/incidents/"]
    :handler {:or [authenticated? can-view?]}}])

(defroutes -routes
  (context "/incidents" []
    (GET "/" request (page-incidents request))))

(def incidents-routes
  (-> #'-routes
      (wrap-access-rules {:rules access-rules
                          :on-error (fn [& _] (make-redirect "/"))})
      middleware/wrap-csrf))
