(ns status-check.routes.auth
  (:require [clojure.tools.logging :as log])
  (:require [bouncer.core :as b]
            [bouncer.validators :as v]
            [buddy.auth :refer [authenticated?]]
            [buddy.auth.accessrules :refer [wrap-access-rules]]
            [compojure.core :refer [defroutes context GET POST]]
            [korma.core :refer [select fields where values sqlfn]]
            [plumbing.core :refer [dissoc-in]])
  (:require [status-check.config :refer [env]]
            [status-check.db.models :as models]
            [status-check.layout :as layout :refer [*identity* make-redirect]]
            [status-check.middleware :as middleware]
            [status-check.sharing.core :as sharing]
            [status-check.users.core :as users]
            [status-check.users.validators :as validators]))

(defn assoc-user [r id] (assoc-in r [:session :identity] id))

(defn dissoc-user [r]
  (dissoc-in r [:session :identity]))

(defn clear-session [r] (assoc r :session {}))

(defn login-page [{:keys [flash]}]
  (layout/render
   "login.html"
   (select-keys flash [:email :password :errors])))

(defn profile-page []
  (let [user (models/user-by-id *identity* :id :email)
        shared-uris (sharing/shared-uris->strings *identity*)]
    (layout/render "profile.html" {:user user
                                   :shared-uris shared-uris})))

(defn signup-page [{:keys [flash]}]
  (layout/render
   "signup.html"
   (merge (select-keys env [:signup-access-token])
          (select-keys flash [:email :password :errors]))))

(defn validate-signup [params]
  (first (b/validate params
                     :email [v/required v/email validators/is-email-free?]
                     :password [v/required [v/min-count 8]])))

(defn validate-login [params]
  (first (b/validate params
                     :email [v/required validators/does-user-exist?]
                     :password [v/required
                                [validators/is-password-correct? (:email params)]])))

(defn logout-handler [request]
  (clear-session (make-redirect "/")))

(defn signup-handler [{:keys [params uri]}]
  (if-let [errors (validate-signup params)]
    (assoc (make-redirect uri) :flash (assoc params :errors errors))
    (let [user (users/create! (:email params) (:password params))]
      (assoc-user (make-redirect "/") (:id user)))))

(defn login-handler [{:keys [params uri]}]
  (if-let [errors (validate-login params)]
    (assoc (make-redirect uri) :flash (assoc params :errors errors))
    (let [user (models/user-by-email (:email params) :id)]
      (assoc-user (make-redirect "/") (:id user)))))

(defn- valid-access-token? [{:keys [params flash]}]
  (if (:signup-access-token env)
    (= (or (:access-token params) (:access-token flash))
       (:signup-access-token env))
    true))

(def access-rules
  [{:uris ["/auth/signup/"]
    :handler {:and [(complement authenticated?)
                    valid-access-token?]}}
   {:uris ["/auth/login/"]
    :handler (complement authenticated?)}
   {:uris ["/auth/logout/" "/auth/profile/"]
    :handler authenticated?}])

(defroutes -routes
  (context
    "/auth" []
    (GET "/login/" request (login-page request))
    (GET "/profile/" request (profile-page))
    (GET "/signup/" request (signup-page request))
    (POST "/login/" request (login-handler request))
    (POST "/logout/" request (logout-handler request))
    (POST "/signup/" request (signup-handler request))))

(def auth-routes
  (-> #'-routes
      (wrap-access-rules {:rules access-rules
                          :on-error (fn [& _] (make-redirect "/"))})
      middleware/wrap-csrf))
