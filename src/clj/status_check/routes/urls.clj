(ns status-check.routes.urls
  (:require [clojure.tools.logging :as log])
  (:require [bouncer.core :as b]
            [bouncer.validators :as v]
            [buddy.auth :refer [authenticated?]]
            [buddy.auth.accessrules :refer [wrap-access-rules]]
            [compojure.core :refer [defroutes context GET POST]]
            [korma.core :refer [values select fields where join] :as korma]
            [ring.util.response :refer [redirect]])
  (:require [status-check.urls.core :as urls]
            [status-check.db.models :as models]
            [status-check.layout :as layout :refer [*identity* make-redirect]]
            [status-check.middleware :as middleware]
            [status-check.sharing.core :as sharing]
            [status-check.utils :refer [parse-int]]))

(def ^:private is-response-page-sharing-enabled? false)

(defn- route-by-url-id [url-id]
  (->> [["/checks/" models/check-id-by-url-id]
        ["/alerters/" models/alerter-id-by-url-id]]
       (map (fn [[route f]]
              (when-let [entity-id (f url-id)]
                (str route entity-id "/"))))
       (filter #(not (nil? %)))
       first))

(defn page-edit-url-parameter [{:keys [route-params]}]
  (layout/render
   "urls/edit-parameter.html"
   (-> route-params
       :parameter-id
       parse-int
       models/url-parameter-by-id)))

(defn page-new-url-parameter [{:keys [flash]}]
  (layout/render
   "urls/new-parameter.html"
   (select-keys flash [:name :value :errors])))

(defn page-url-responses [{:keys [route-params]}]
  ;; TODO Unittest
  (layout/render
   "urls/responses.html"
   {:responses (-> route-params :id parse-int
                   urls/responses
                   urls/pretty-print-responses)}))

(defn page-url-response [{:keys [route-params]}]
  ;; TODO Unittest
  (layout/render
   "urls/response.html"
   {:response
    (urls/pretty-print-response
     (urls/response (-> route-params :id parse-int)
                    (-> route-params :response-id parse-int)))}))

(defn validate-url-parameter [params]
  (first (b/validate params
                     :name [v/required [v/max-count 255]]
                     :value [v/required [v/max-count 255]])))

(defn handle-update-url-parameter [{:keys [route-params params uri]}]
  (if-let [errors (validate-url-parameter params)]
    (assoc (redirect uri) :flash (assoc params :errors errors))
    (do (urls/update-parameter! (parse-int (:parameter-id route-params))
                                (:name params)
                                (:value params))
        (make-redirect (route-by-url-id
                        (-> route-params :id parse-int))))))

(defn handle-new-url-parameter [{:keys [params uri]}]
  (if-let [errors (validate-url-parameter params)]
    (assoc (redirect uri) :flash (assoc params :errors errors))
    (let [url-id (parse-int (:id params))]
      (urls/create-parameter! url-id (:name params) (:value params))
      (make-redirect (route-by-url-id url-id)))))

(defn owner? [{:keys [match-params]}]
  (urls/owner? *identity* (-> match-params :id parse-int)))

(defn- can-view? [{:keys [match-params params uri]}]
  ;; TODO Unittest
  (let [id (-> match-params :id parse-int)
        user-id (-> params :user-id parse-int)
        access-token (:access-token params)]
    (and is-response-page-sharing-enabled?
         user-id
         access-token
         (urls/owner? user-id id)
         (sharing/authorized? user-id uri access-token))))

(def access-rules
  [{:uris ["/urls/:id/parameters/:parameter-id/"
           "/urls/:id/parameters/new/"
           "/urls/:id/responses/"  ;; TODO Unittest
           ]
    :handler {:and [authenticated? owner?]}}
   {:uris ["/urls/:id/responses/:response-id/"]  ;; TODO Unittest
    :handler {:or [can-view? {:and [authenticated? owner?]}]}}])

(defroutes -routes
  (context "/urls/:id{[0-9]+}" []

    (GET "/parameters/:parameter-id{[0-9]+}/"
      request (page-edit-url-parameter request))
    (GET "/parameters/new/" request (page-new-url-parameter request))

    (GET "/responses/" request (page-url-responses request))
    (GET "/responses/:response-id{[0-9]+}/"
      request (page-url-response request))

    (POST "/parameters/:parameter-id{[0-9]+}/"
      request (handle-update-url-parameter request))
    (POST "/parameters/new/" request (handle-new-url-parameter request))))

(def urls-routes
  (-> #'-routes
      (wrap-access-rules {:rules access-rules
                          :on-error (fn [& _] (make-redirect "/"))})
      middleware/wrap-csrf))
