(ns status-check.routes.core
  (:require [compojure.core :refer [defroutes GET]])
  (:require [status-check.layout :as layout :refer [*identity* make-redirect]]
            [status-check.middleware :as middleware]))

(defn- index-page []
  (if (nil? *identity*)
    (layout/render "index.html")
    (make-redirect (str "/summary/"))))

(defn- about-page []
  (layout/render "about.html"))

(defroutes -routes
  (GET "/" [] (index-page))
  (GET "/about/" [] (about-page)))

(def core-routes
  (-> #'-routes
      middleware/wrap-csrf))
