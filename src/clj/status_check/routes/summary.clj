(ns status-check.routes.summary
  (:require [clojure.string :as str])
  (:require [buddy.auth :refer [authenticated?]]
            [buddy.auth.accessrules :refer [wrap-access-rules]]
            [compojure.core :refer [defroutes GET]]
            [korma.core :refer [select where]])
  (:require [status-check.urls.core :refer [okay-response-codes]]
            [status-check.db.models :as models]
            [status-check.layout :as layout :refer [make-redirect *identity*]]
            [status-check.middleware :as middleware]))

(defn summary-page [request]
  (letfn [(prepare-summary [summary]
            (->> summary
                 (map #(assoc % :is-status-ok
                              (contains? okay-response-codes (:last-status %))))
                 (filter (complement :is-status-ok))
                 (sort-by (juxt (complement :is-active)
                                :escalation-point-seconds
                                #(str/lower-case (:title %))))))]
    (let [user (models/user-by-id *identity*)
          incidents-summary
          (select models/incidents-summary
                  (where {:owner-id *identity* :is-closed false}))
          checks-summary
          (prepare-summary
           (select models/checks-summary (where {:owner-id *identity*})))
          alerters-summary
          (prepare-summary
           (select models/alerters-summary (where {:owner-id *identity*})))]
      (layout/render "summary.html" {:user user
                                     :incidents-summary incidents-summary
                                     :checks-summary checks-summary
                                     :alerters-summary alerters-summary}))))

(defroutes -routes
  (GET "/summary/" request (summary-page request)))

(def access-rules
  [{:uris ["/summary/"]
    :handler authenticated?}])

(def summary-routes
  (-> #'-routes
      (wrap-access-rules {:rules access-rules
                          :on-error (fn [& _] (make-redirect "/"))})
      middleware/wrap-csrf))
