(ns status-check.routes.alerters
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.string :as str]
            [clojure.tools.logging :as log])
  (:require [bouncer.core :as b]
            [bouncer.validators :as v]
            [buddy.auth :refer [authenticated?]]
            [buddy.auth.accessrules :refer [wrap-access-rules]]
            [compojure.core :refer [defroutes context GET POST]]
            [korma.db :refer [transaction]]
            [korma.core :refer [values select fields where set-fields] :as korma]
            [ring.util.response :refer [redirect]])
  (:require [status-check.alerters.core :as alerters]
            [status-check.alerters.types :as alerters-types]
            [status-check.alerters.validators :as alerters-validators]
            [status-check.urls.core :refer [okay-response-codes]]
            [status-check.db.core :as db]
            [status-check.db.models :as models]
            [status-check.layout :as layout :refer [*identity* make-redirect]]
            [status-check.middleware :as middleware]
            [status-check.utils :refer [parse-int]]
            [status-check.validators :as validators]))

(defn page-alerters [params]
  ;; TODO Unittest
  (letfn [(prepare-summary [summary]
            (->> summary
                 (map #(assoc % :is-status-ok (contains? okay-response-codes (:last-status %))))
                 (sort-by (juxt (complement :is-active)
                                :escalation-point-seconds
                                #(str/lower-case (:title %))))))]
    (let [user (models/user-by-id *identity*)
          alerters-summary
          (prepare-summary
           (select models/alerters-summary (where {:owner-id *identity*})))]
      (layout/render "alerters/index.html" {:user user
                                            :alerters-summary alerters-summary}))))

(defn page-edit-alerter [{:keys [params flash]}]
  (let [alerter (models/alerter-by-id (parse-int (:id params)))
        alerter-types (map (juxt :id :name) (select models/alerter-types))
        url (models/url-by-id (:url-id alerter))
        url-parameters (models/url-parameters-by-id (:id url))
        slack-alerter? (= (:type-name alerter) alerters-types/type-name-slack)]
    (layout/render
     "alerters/edit.html"
     (merge alerter
            (select-keys url [:http-method :url])
            (select-keys flash [:errors])
            {:url-parameters url-parameters
             :alerter-types alerter-types
             :slack-alerter? slack-alerter?}))))

(defn page-new-alerter [{:keys [flash]}]
  (layout/render
   "alerters/new.html"
   (select-keys flash [:title :errors])))

(defn validate-update-alerter [params]
  ;; TODO Unittests
  (first (b/validate params
                     :title [v/required [v/max-count 255]]
                     :type [v/required alerters-validators/alerter-type?]
                     :http-method [v/required validators/http-method?]
                     :escalation-point-seconds [v/required
                                                validators/contains-integer?
                                                validators/non-negative-integer?]
                     :url [v/required [v/max-count 255]])))

(defn handle-update-alerter [{:keys [params uri]}]
  (log/debug "routes.alerters/handle-update-alerter" (:type params))
  (if-let [errors (validate-update-alerter params)]
    (assoc (redirect uri) :flash (assoc params :errors errors))
    (do (alerters/update! (parse-int (:id params))
                          (contains? params :is-active)
                          (:title params)
                          (:type params)
                          (:http-method params)
                          (:url params)
                          (parse-int (:escalation-point-seconds params)))
        (redirect uri))))

(defn validate-new-alerter [params]
  (first (b/validate params :title [v/required [v/max-count 255]])))

(defn handle-new-alerter [{:keys [params uri]}]
  (if-let [errors (validate-new-alerter params)]
    (assoc (redirect uri) :flash (assoc params :errors errors))
    (->> (alerters/create! *identity* (:title params))
         :id
         (str "/alerters/")
         make-redirect)))

(defn owner? [{:keys [match-params]}]
  (alerters/owner? *identity* (-> match-params :id parse-int)))

(def access-rules
  [{:uris ["/alerters/"] ;; TODO Unittest
    :handler authenticated?}
   {:uris ["/alerters/new/"]
    :handler authenticated?}
   {:uris ["/alerters/:id/"]
    :handler {:and [authenticated? owner?]}}])

(defroutes -routes
  (context "/alerters" []
    (GET "/" request (page-alerters request))
    (GET "/:id{[0-9]+}/" request (page-edit-alerter request))
    (GET "/new/" request (page-new-alerter request))
    (POST "/:id{[0-9]+}/" request (handle-update-alerter request))
    (POST "/new/" request (handle-new-alerter request))))

(def alerters-routes
  (-> #'-routes
      (wrap-access-rules {:rules access-rules
                          :on-error (fn [& _] (make-redirect "/"))})
      middleware/wrap-csrf))

