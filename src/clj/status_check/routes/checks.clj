(ns status-check.routes.checks
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.string :as str]
            [clojure.tools.logging :as log])
  (:require [bouncer.core :as b]
            [bouncer.validators :as v]
            [buddy.auth :refer [authenticated?]]
            [buddy.auth.accessrules :refer [wrap-access-rules]]
            [compojure.core :refer [defroutes context GET POST]]
            [ring.util.response :refer [redirect]]
            [korma.core :refer [values select fields where set-fields] :as korma])
  (:require [status-check.alerters.validators :as alerters-validators]
            [status-check.urls.core :refer [okay-response-codes]]
            [status-check.db.core :as db]
            [status-check.db.models :as models]
            [status-check.layout :as layout :refer [*identity* make-redirect]]
            [status-check.middleware :as middleware]
            [status-check.checks.core :as checks]
            [status-check.sharing.core :as sharing]
            [status-check.utils :refer [parse-int]]
            [status-check.validators :as validators]))

(defn page-checks [{:keys [uri params]}]
  (letfn [(prepare-summary [summary]
            (->> summary
                 (map #(assoc % :is-status-ok
                              (contains? okay-response-codes (:last-status %))))
                 (sort-by (juxt (complement :is-active)
                                :is-status-ok
                                #(str/lower-case (:title %))))))]
    (let [user-id (or *identity* (parse-int (:user-id params)))
          user (models/user-by-id user-id)
          shared-uris (sharing/shared-uris->strings user-id uri)
          checks-summary
          (prepare-summary
           (select models/checks-summary (where {:owner-id user-id})))]
      (layout/render "checks/index.html" {:user user
                                          :shared-uris shared-uris
                                          :checks-summary checks-summary}))))

(defn page-edit-check [{:keys [params flash]}]
  (let [check (models/check-by-id (parse-int (:id params)))
        url (models/url-by-id (:url-id check))
        url-parameters (models/url-parameters-by-id (:id url))]
    (layout/render
     "checks/edit.html"
     (merge check
            (select-keys url [:http-method :url])
            (select-keys flash [:errors])
            {:url-parameters url-parameters}))))

(defn page-new-check [{:keys [flash]}]
  (layout/render
   "checks/new.html"
   (select-keys flash [:title :errors])))

(defn validate-update-check [params]
  (first (b/validate params
                     :title [v/required [v/max-count 255]]
                     :check-interval [v/required validators/contains-integer?]
                     :http-method [v/required validators/http-method?]
                     :url [v/required [v/max-count 255]])))

(defn handle-update-check [{:keys [params uri]}]
  (if-let [errors (validate-update-check params)]
    (assoc (redirect uri) :flash (assoc params :errors errors))
    (do (checks/update! (parse-int (:id params))
                        (contains? params :is-active)
                        (:title params)
                        (->> params :check-interval parse-int)
                        (:http-method params)
                        (:url params))
        (redirect uri))))

(defn validate-new-check [params]
  (first (b/validate params :title [v/required [v/max-count 255]])))

(defn handle-new-check [{:keys [params uri]}]
  (if-let [errors (validate-new-check params)]
    (assoc (redirect uri) :flash (assoc params :errors errors))
    (->> (checks/create! *identity* (:title params))
         :id
         (str "/checks/")
         make-redirect)))

(defn owner? [{:keys [match-params]}]
  (checks/owner? *identity* (-> match-params :id parse-int)))

(defn- can-view? [{:keys [params uri]}]
  (and (:user-id params)
       (:access-token params)
       (sharing/authorized? (parse-int (:user-id params))
                            uri (:access-token params))))

(def access-rules
  [{:uris ["/checks/"]
    :handler {:or [authenticated? can-view?]}}
   {:uris ["/checks/new/"]
    :handler authenticated?}
   {:uris ["/checks/:id/"]
    :handler {:and [authenticated? owner?]}}])

(defroutes -routes
  (context "/checks" []
    (GET "/" request (page-checks request))
    (GET "/:id{[0-9]+}/" request (page-edit-check request))
    (GET "/new/" request (page-new-check request))
    (POST "/:id{[0-9]+}/" request (handle-update-check request))
    (POST "/new/" request (handle-new-check request))))

(def checks-routes
  (-> #'-routes
      (wrap-access-rules {:rules access-rules
                          :on-error (fn [& _] (make-redirect "/"))})
      middleware/wrap-csrf))
