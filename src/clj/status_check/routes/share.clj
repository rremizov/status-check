(ns status-check.routes.share
  (:require [clojure.tools.logging :as log])
  (:require [buddy.auth :refer [authenticated?]]
            [buddy.auth.accessrules :refer [wrap-access-rules]]
            [compojure.core :refer [defroutes context POST]])
  (:require [status-check.layout :as layout :refer [*identity* make-redirect]]
            [status-check.middleware :as middleware]
            [status-check.sharing.core :as sharing]
            [status-check.utils :refer [parse-int]]))

(defn handle-share-uri [{:keys [params]}]
  (do (sharing/share-uri! *identity* (:uri params))
      (make-redirect (str "/auth/profile/"))))

(defn handle-delete-shared-uri [{:keys [params]}]
  (do (sharing/delete-shared-uri! (parse-int (:id params)))
      (make-redirect (str "/auth/profile/"))))

(defn owner? [{:keys [match-params]}]
  (sharing/owner? *identity* (-> match-params :id parse-int)))

(def access-rules
  [{:uris ["/share/uri/"]
    :handler authenticated?}
   {:uris ["/share/uri/:id/delete/"]
    :handler {:and [authenticated? owner?]}}])

(defroutes -routes
  (context
    "/share" []
    (POST "/uri/" request (handle-share-uri request))
    (POST "/uri/:id{[0-9]+}/delete/" request (handle-delete-shared-uri request))))

(def share-routes
  (-> #'-routes
      (wrap-access-rules {:rules access-rules
                          :on-error (fn [& _] (make-redirect "/"))})
      middleware/wrap-csrf))
