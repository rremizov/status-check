(ns status-check.db.models
  (:require [korma.core :refer :all]))

(defentity users)

(defentity alerters)
(defentity alerters-summary)
(defentity alerter-alert)
(defentity alerter-types)

(defentity checks)
(defentity checks-summary)
(defentity checks-statuses-changes)

(defentity url-parameters)
(defentity urls
  (has-many url-parameters {:fk :url-id}))
(defentity url-response-headers)
(defentity url-responses
  (has-many url-response-headers {:fk :url-response-id}))
(defentity urls-summary)

(defentity incidents)
(defentity incidents-summary)

(defentity shared-uris)

(defn is-email-free? [v]
  (-> (select users (fields :id) (where {:email v}))
      first :id nil?))

(defn select-user
  ([where-clause] (select-user where-clause :id :email :last-login :is-active))
  ([where-clause & select-fields]
   (-> (select* users)
       (#(apply fields % select-fields))
       (where where-clause)
       select
       first)))

(defn user-by-id [id & select-fields]
  (apply select-user {:id id} select-fields))
(defn user-by-email [email & select-fields]
  (apply select-user {:email email} select-fields))

(def active-alerters
  (-> (select* alerters)
      (fields :alerters.id
              :alerters.owner-id
              :alerters.type-id
              :alerters.url-id
              :alerters.title
              :alerters.escalation-point-seconds
              :alerters.is-active
              :alerters.created-at
              [:alerter-types.name :type-name])
      (join alerter-types (= :alerters.type-id :alerter-types.id))))

(def checks-with-urls
  (-> (select* checks)
      (fields :checks.id :checks.owner-id :checks.title :checks.is-active
              :checks.check-interval :checks.url-id
              :urls.http-method :urls.url)
      (join urls
            (= :checks.url-id :urls.id))))

(defn active-checks []
  (where checks-with-urls {:checks.is-active true}))

(defn checks-by-owner-id [owner-id]
  (select checks-with-urls (where {:checks.owner-id owner-id})))

(defn check-by-id [id]
  (first (select checks
                 (fields :id :owner-id :url-id
                         :title :check-interval :is-active)
                 (where {:id id}))))

(defn check-by-url-id [id]
  (first (select urls
                 (fields :checks.id :checks.owner-id)
                 (join checks
                       (= :checks.url-id :urls.id))
                 (where (= :urls.id id)))))

(defn check-id-by-url-id [id]
  (:id (check-by-url-id id)))

(defn alerter-by-id [id]
  (first (select alerters
                 (fields :id
                         :url-id
                         :title
                         :type-id
                         :escalation-point-seconds
                         :is-active
                         [:alerter-types.name :type-name])
                 (join alerter-types (= :alerters.type-id :alerter-types.id))
                 (where {:id id}))))

(defn alerter-type-by-name [name]
  (first (select alerter-types (where {:name name}))))

(defn alerter-by-url-id [id]
  (first (select urls
                 (fields :alerters.id :alerters.owner-id)
                 (join alerters
                       (= :alerters.url-id :urls.id))
                 (where (= :urls.id id)))))

(defn alerter-id-by-url-id [id]
  (:id (alerter-by-url-id id)))

(defn url-by-id [id]
  (first (select urls
                 (fields :id :http-method :url)
                 (where {:id id}))))

(defn url-parameters-by-id [url-id]
  (select url-parameters
          (fields :id :name :value)
          (where {:url-id url-id})))

(defn url-parameter-by-id [id]
  (first (select url-parameters
                 (fields :name :value)
                 (where {:id id}))))
