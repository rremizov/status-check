(ns status-check.db.core
  (:require [clojure.tools.logging :as log])
  (:require [clojure.java.jdbc :as jdbc]
            [to-jdbc-uri.core :refer [to-jdbc-uri]])
  (:require [camel-snake-kebab.core :refer [->kebab-case ->snake_case ->kebab-case-keyword]]
            [camel-snake-kebab.extras :refer [transform-keys]]
            [cheshire.core :refer [parse-string]]
            [conman.core :as conman]
            [korma.core]
            [korma.db]
            [mount.core :refer [defstate]])
  (:require [status-check.config :refer [env]]
            [status-check.db.utils :as dbutils]
            [status-check.utils :as utils])
  (:import [status_check.utils TimeInterval])
  (:import org.postgresql.util.PGobject
           org.postgresql.util.PGInterval
           org.postgresql.jdbc4.Jdbc4Array
           clojure.lang.IPersistentMap
           clojure.lang.IPersistentVector
           [java.sql
            BatchUpdateException
            Date
            Timestamp
            PreparedStatement]))

(defn connect-db! []
  (let [conn (conman/connect!
              {:init-size  1
               :min-idle   1
               :max-idle   4
               :max-active 8
               :driver-class-name "org.postgresql.Driver"
               :jdbc-url   (to-jdbc-uri (or (env :status-check-database-url)
                                            (env :database-url)))})]

    ;; Source: Use kebab-case in the sources and snake_case in the database
    ;; https://korma.github.io/Korma/korma.config.html#var-set-naming
    (korma.config/set-naming {:keys ->kebab-case :fields ->snake_case})

    (korma.db/default-connection conn)
    conn))

(defstate ^:dynamic *db*
  :start (connect-db!)
  :stop (conman/disconnect! *db*))

;; --- HugSQL
(conman/bind-connection *db* "sql/queries.sql")

;; Use kebab-case in the sources and snake_case in the database
;; Source: http://www.luminusweb.net/docs/database.md
(defn result-one-snake->kebab
  [this result options]
  (transform-keys ->kebab-case-keyword
                  (hugsql.adapter/result-one this result options)))

(defn result-many-snake->kebab
  [this result options]
  (map #(transform-keys ->kebab-case-keyword %)
       (hugsql.adapter/result-many this result options)))

(defmethod hugsql.core/hugsql-result-fn :1 [sym]
  'status-check.db.core/result-one-snake->kebab)

(defmethod hugsql.core/hugsql-result-fn :one [sym]
  'status-check.db.core/result-one-snake->kebab)

(defmethod hugsql.core/hugsql-result-fn :* [sym]
  'status-check.db.core/result-many-snake->kebab)

(defmethod hugsql.core/hugsql-result-fn :many [sym]
  'status-check.db.core/result-many-snake->kebab)
;; --- HugSQL

(extend-protocol jdbc/IResultSetReadColumn
  Date
  (result-set-read-column [v _ _] (dbutils/sqldate->javadate v))

  Timestamp
  (result-set-read-column [v _ _] (dbutils/sqldate->javadate v))

  Jdbc4Array
  (result-set-read-column [v _ _] (vec (.getArray v)))

  PGobject
  (result-set-read-column [pgobj _metadata _index]
    (let [type  (.getType pgobj)
          value (.getValue pgobj)]
      (case type
        "json" (parse-string value true)
        "jsonb" (parse-string value true)
        "citext" (str value)
        value)))

  PGInterval
  (result-set-read-column [v _ _]
    (utils/->TimeInterval (.getSeconds v)
                          (.getMinutes v)
                          (.getHours v)
                          (.getDays v)
                          (.getMonths v)
                          (.getYears v))))

(extend-type java.util.Date
  jdbc/ISQLParameter
  (set-parameter [v ^PreparedStatement stmt ^long idx]
    (.setTimestamp stmt idx (Timestamp. (.getTime v)))))

(extend-type clojure.lang.IPersistentVector
  jdbc/ISQLParameter
  (set-parameter [v ^java.sql.PreparedStatement stmt ^long idx]
    (let [conn      (.getConnection stmt)
          meta      (.getParameterMetaData stmt)
          type-name (.getParameterTypeName meta idx)]
      (if-let [elem-type
               (when (= (first type-name) \_)
                 (clojure.string/join (rest type-name)))]
        (.setObject stmt idx (.createArrayOf conn elem-type (to-array v)))
        (.setObject stmt idx (dbutils/->pgjsonb v))))))

(extend-protocol jdbc/ISQLValue
  TimeInterval
  (sql-value [value] (dbutils/map->pginterval value))
  IPersistentMap
  (sql-value [value] (dbutils/->pgjsonb value))
  IPersistentVector
  (sql-value [value] (dbutils/->pgjsonb value)))

