(ns status-check.db.utils
  (:require [camel-snake-kebab.core :refer [->kebab-case-keyword]]
            [camel-snake-kebab.extras :refer [transform-keys]]
            [cheshire.core :refer [generate-string]]
            [korma.core :as korma])
  (:import org.postgresql.util.PGobject
           org.postgresql.util.PGInterval))

(defn sqldate->javadate [sql-date]
  (-> sql-date (.getTime) (java.util.Date.)))

(defn str->pgobject [type value]
  (doto (PGobject.)
    (.setType type)
    (.setValue value)))

(def ->pgjson (comp (partial str->pgobject "json") generate-string))
(def ->pgjsonb (comp (partial str->pgobject "jsonb") generate-string))

(defn map->pginterval [value]
  (doto (PGInterval.)
    (.setSeconds (get value :seconds 0))
    (.setMinutes (get value :minutes 0))
    (.setHours (get value :hours 0))
    (.setDays (get value :days 0))
    (.setMonths (get value :months 0))
    (.setYears (get value :years 0))))

(defmacro kinsert [ent & body]
  `(transform-keys ->kebab-case-keyword (korma/insert ~ent ~@body)))
