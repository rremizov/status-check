(ns status-check.scheduler.triggers
  (:require [clojurewerkz.quartzite.triggers :as t]
            [clojurewerkz.quartzite.schedule.simple :as s]))

(defn build-periodic-trigger [identity-key seconds]
  (t/build (t/with-identity (t/key identity-key))
           (t/start-now)
           (t/with-schedule (s/schedule (s/repeat-forever)
                                        (s/with-interval-in-seconds seconds)))))

