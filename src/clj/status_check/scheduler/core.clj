(ns status-check.scheduler.core
  (:require [clojure.tools.logging :as log])
  (:require [clojurewerkz.quartzite.scheduler :as qs]
            [mount.core :refer [defstate]]))

(defn- start []
  (log/info "Starting quartzite scheduler...")
  (qs/start (qs/initialize)))

(defstate scheduler
  :start (start)
  :stop (qs/shutdown scheduler))

