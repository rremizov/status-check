(ns status-check.scheduler.jobs
  (:require [clojure.tools.logging :as log])
  (:require [clojurewerkz.quartzite.conversion :as qc]
            [clojurewerkz.quartzite.jobs :as j]))

(defn build-job [identity-key type payload]
  (log/debug "build-job" identity-key type payload)
  (j/build (j/of-type type)
           (j/using-job-data payload)
           (j/with-identity (j/key identity-key))))

