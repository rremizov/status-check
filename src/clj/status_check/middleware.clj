(ns status-check.middleware
  (:require [clojure.string :as str]
            [clojure.tools.logging :as log])
  (:require [buddy.auth :refer [authenticated?]]
            [buddy.auth.accessrules :refer [restrict]]
            [buddy.auth.backends.session :refer [session-backend]]
            [buddy.auth.middleware :refer [wrap-authentication wrap-authorization]]
            [immutant.web.middleware :refer [wrap-session]]
            [ring.middleware.anti-forgery :refer [wrap-anti-forgery]]
            [ring.middleware.defaults :refer [secure-site-defaults site-defaults wrap-defaults]]
            [ring.middleware.flash :refer [wrap-flash]]
            [ring.middleware.format :refer [wrap-restful-format]]
            [ring.middleware.webjars :refer [wrap-webjars]])
  (:require [status-check.config :refer [env]]
            [status-check.env :refer [defaults]]
            [status-check.layout :refer [*app-context* error-page]]
            [status-check.layout :refer [*identity*]])
  (:import [javax.servlet ServletContext]))

(defn wrap-context [handler]
  (fn [request]
    (binding [*app-context*
              (if-let [context (:servlet-context request)]
                ;; If we're not inside a servlet environment
                ;; (for example when using mock requests), then
                ;; .getContextPath might not exist
                (try (do (log/debug "WRAP-CONTEXT" (.getContextPath ^ServletContext context))
                         (.getContextPath ^ServletContext context))
                     (catch IllegalArgumentException _ context))
                ;; if the context is not specified in the request
                ;; we check if one has been specified in the environment
                ;; instead
                (do (log/debug "WRAP-CONTEXT" (:app-context env))
                    (:app-context env)))]
      (handler request))))

(defn wrap-internal-error [handler]
  (fn [req]
    (try
      (handler req)
      (catch Throwable t
        (log/error t)
        (error-page {:status 500
                     :title "Something very bad has happened!"
                     :message "We've dispatched a team of highly trained gnomes to take care of the problem."})))))

(defn wrap-csrf [handler]
  (wrap-anti-forgery
   handler
   {:error-response
    (error-page
     {:status 403
      :title "Invalid anti-forgery token"})}))

(defn wrap-formats [handler]
  (let [wrapped (wrap-restful-format
                 handler
                 {:formats [:json-kw :transit-json :transit-msgpack]})]
    (fn [request]
      ;; disable wrap-formats for websockets
      ;; since they're not compatible with this middleware
      ((if (:websocket? request) handler wrapped) request))))

(defn on-error [request response]
  (error-page
   {:status 403
    :title (str "Access to " (:uri request) " is not authorized")}))

(defn wrap-restricted [handler]
  (restrict handler {:handler authenticated?
                     :on-error on-error}))

(defn wrap-identity [handler]
  (fn [request]
    (binding [*identity* (get-in request [:session :identity])]
      (handler request))))

(defn wrap-auth [handler]
  (let [backend (session-backend)]
    (-> handler
        wrap-identity
        (wrap-authentication backend)
        (wrap-authorization backend))))

(defn wrap-redirect-trailing-slash
  "If the requested url has not a trailing slash, append it."
  [handler]
  (fn [request]
    (if (.endsWith (request :uri) "/")
      (handler request)
      (ring.util.http-response/found (str (request :uri) "/")))))

(defn wrap-base [handler]
  (-> ((:middleware defaults) handler)
      wrap-redirect-trailing-slash
      wrap-auth
      wrap-formats
      wrap-webjars
      wrap-flash
      (wrap-session {:cookie-attrs {:http-only true}})
      (wrap-defaults
       (-> (if (or (:dev env) (:test env)) site-defaults secure-site-defaults)
           (assoc-in [:security :anti-forgery] false)
           (assoc-in [:security :ssl-redirect] false)
           (assoc-in [:responses :absolute-redirects] false)
           (dissoc :session)))
      wrap-context
      wrap-internal-error))
