(ns status-check.layout
  (:require [clojure.string :as str]
            [clojure.tools.logging :as log])
  (:require [markdown.core :refer [md-to-html-string]]
            [ring.middleware.anti-forgery :refer [*anti-forgery-token*]]
            [ring.util.anti-forgery :refer [anti-forgery-field]]
            [ring.util.http-response :refer [content-type ok]]
            [ring.util.response :refer [redirect]]
            [selmer.filters :as filters]
            [selmer.parser :as parser])
  (:require [status-check.filters]
            [status-check.utils :as utils]))

(declare ^:dynamic *identity*)
(declare ^:dynamic *app-context*)
(parser/set-resource-path!  (clojure.java.io/resource "templates"))
(parser/add-tag! :csrf-field (fn [_ _] (anti-forgery-field)))
(filters/add-filter! :markdown (fn [content] [:safe (md-to-html-string content)]))
(filters/add-filter! :interval-to-seconds #'utils/interval->seconds)
(filters/add-filter! :round #(utils/round %))
(filters/add-filter! :boolean-to-glyph #'status-check.filters/boolean->glyph)
(filters/add-filter! :map-key key)
(filters/add-filter! :map-val val)
(filters/add-filter! :clj-name name)
(filters/add-filter! :datetime utils/date->str)

(defn make-url [& more] (str/join (cons *app-context* more)))
(defn make-redirect [& more]

  ;; TODO
  ;; "Location: /" somehow becomes Location: http://host:port/
  ;; ring-defaults does that: https://github.com/ring-clojure/ring-defaults#customizing
  ;; https://github.com/ring-clojure/ring-defaults/search?q=absolute-redirects&unscoped_q=absolute-redirects
  (log/debug "MAKE-REDIRECT" (redirect "/"))  ;; {:status 302, :headers {Location /}, :body }

  (->> more (apply make-url) redirect))

(defn render
  "renders the HTML template located relative to resources/templates"
  [template & [params]]
  (content-type
   (ok
    (parser/render-file
     template
     (assoc params
            :authenticated? (not (nil? *identity*))
            :page template
            :csrf-token *anti-forgery-token*
            :servlet-context *app-context*)))
   "text/html; charset=utf-8"))

(defn error-page
  "error-details should be a map containing the following keys:
   :status - error status
   :title - error title (optional)
   :message - detailed error message (optional)

   returns a response map with the error page as the body
   and the status specified by the status key"
  [error-details]
  {:status  (:status error-details)
   :headers {"Content-Type" "text/html; charset=utf-8"}
   :body    (parser/render-file "error.html" error-details)})
