(ns status-check.users.validators
  (:require [bouncer.core :as b]
            [bouncer.validators :as v]
            [buddy.hashers :as hashers])
  (:require [status-check.db.models :as models]
            [status-check.utils :refer [parse-int]]))

(v/defvalidator is-email-free?
  {:default-message-format "%s registered already"}
  [p]
  (models/is-email-free? p))

(v/defvalidator does-user-exist?
  {:default-message-format "user %s does not exist"}
  [p]
  (not (models/is-email-free? p)))

(v/defvalidator is-password-correct?
  {:default-message-format "password is incorrect"}
  [password email]
  (->> (models/user-by-email email :password)
       :password
       (hashers/check password)))

