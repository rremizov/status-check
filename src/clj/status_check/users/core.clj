(ns status-check.users.core
  (:require [buddy.hashers :as hashers]
            [korma.core :refer [select fields where set-fields values] :as korma]
            [korma.db :refer [transaction]])
  (:require [status-check.db.models :as models]
            [status-check.db.utils :refer [kinsert]]))

(defn create! [email password]
  (kinsert models/users
           (values {:email email
                    :password (hashers/encrypt password)})))

