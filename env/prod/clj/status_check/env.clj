(ns status-check.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[status-check started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[status-check has shut down successfully]=-"))
   :middleware identity})
