(ns status-check.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [status-check.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[status-check started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[status-check has shut down successfully]=-"))
   :middleware wrap-dev})
