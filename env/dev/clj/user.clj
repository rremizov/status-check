(ns user
  (:require [mount.core :as mount]
            status-check.core))

(defn start []
  (mount/start-without #'status-check.core/repl-server))

(defn stop []
  (mount/stop-except #'status-check.core/repl-server))

(defn restart []
  (stop)
  (start))


