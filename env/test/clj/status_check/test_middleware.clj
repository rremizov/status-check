(ns status-check.test-middleware
  (:require [ring.middleware.reload :refer [wrap-reload]]
            [selmer.middleware :refer [wrap-error-page]]
            [prone.middleware :refer [wrap-exceptions]])
  (:require [status-check.layout]))

(def ^:dynamic *identity* nil)

(defn wrap-identity [handler]
  (fn [request]
    (if (nil? *identity*)
      (handler request)
      (binding [status-check.layout/*identity* *identity*]
        (-> request
            (assoc :identity *identity*)
            (assoc-in [:session :identity] *identity*)
            handler)))))

(defn wrap-test [handler]
  (-> handler
      wrap-identity))

