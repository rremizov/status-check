(ns status-check.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [status-check.test-middleware :refer [wrap-test]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[status-check started successfully using the testing profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[status-check has shut down successfully]=-"))
   :middleware wrap-test})

