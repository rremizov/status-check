(ns status-check.test.users.core
  (:require [clojure.test :refer [use-fixtures is]])
  (:require [status-check.users.core :as core]
            [status-check.test.core :refer [defdbtest]]
            [status-check.test.fixtures :as fixtures]
            [status-check.test.generators :as g]))

(use-fixtures :once fixtures/app)

(defdbtest test-create!
  (let [user (core/create! (g/email) (g/password))]
    (is (not (nil? user)))
    (is (contains? user :id))
    (is (contains? user :email))
    (is (contains? user :password))
    (is (contains? user :last-login))
    (is (contains? user :is-active))))

