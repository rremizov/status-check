(ns status-check.test.generators
  (:require [clojure.data.generators :as g]
            [clojure.tools.logging :as log])
  (:require [buddy.hashers :as hashers]
            [korma.core :as k])
  (:require [status-check.db.core :as db]
            [status-check.db.migrations :as migrations]
            [status-check.db.models :as models]
            [status-check.db.utils :refer [kinsert]]
            [status-check.utils :as utils])
  (:import [java.util UUID]))

(def ^:private ascii-letters
  (let [letters "abcdefghijklmnopqrstuvwxyz"]
    (map clojure.core/char
         (str letters
              (clojure.string/upper-case letters)))))

(def ascii-letter (partial rand-nth ascii-letters))
(def string (partial g/string ascii-letter 10))
(def stringl (partial g/string ascii-letter))
(def boolean g/boolean)
(def http-method (partial rand-nth (seq utils/http-methods)))
(def domain-zone
  (partial rand-nth
           ["com" "edu" "gov" "io" "net"]))
(def http-response-status
  (partial rand-nth
           [200 201 300 301 302 400 401 402 403 404 500]))

(defn url-address []
  (str (rand-nth ["http" "https"])
       "://"
       (stringl 50)
       "."
       (stringl 50)
       "."
       (domain-zone)))

(defn email []
  (str (stringl 50)
       "@"
       (stringl 50)
       "."
       (domain-zone)))

(def password string)

(defn json
  ([] (json 2))
  ([depth]
   {:pre [pos? depth]}
   (let [n-keys (rand-int 10)
         keys (repeatedly n-keys string)
         values (if (= 1 depth)
                  (repeatedly n-keys string)
                  (repeatedly n-keys #(json (dec depth))))]
     (zipmap keys values))))

(defn http-response [&_]
  {:status (http-response-status)
   :headers (json 1)
   :body (string)
   :request-time (rand-int 1000)})

(declare check)

(defn url
  ([]
   (kinsert models/urls
            (k/values {:http-method (http-method)
                       :url (url-address)})))
  ([owner]
   (let [url (url)]
     (check owner url)
     url)))

(defn url-parameter
  ([] (url-parameter (url)))
  ([url]
   (kinsert models/url-parameters
            (k/values {:url-id (:id url)
                       :name (stringl 255)
                       :value (stringl 255)}))))

(defn url-response
  ([] (url-response (url)))
  ([url]
   (let [values {:url-id (:id url)
                 :status (http-response-status)
                 :body (string)
                 :request-time (rand-int 1000)
                 :headers (json 1)}]
     (db/save-url-response<! values))))

(defn user
  ([] (user (email) (password)))
  ([email password]
   (kinsert models/users
            (k/values {:email email
                       :password (hashers/encrypt password)}))))

(defn check
  ([] (check (user) (url)))
  ([owner] (check owner (url)))
  ([owner url]
   (kinsert models/checks
            (k/values {:owner-id (:id owner)
                       :title (stringl 255)
                       :url-id (:id url)}))))

(defn alerter-type []
  (let [{:keys [id name]}
        (rand-nth (k/select models/alerter-types))]
    [id name]))

(defn alerter-type-name []
  (let [[id name] (alerter-type)] name))

(defn alerter
  ([] (alerter (user) (url)))
  ([owner] (alerter owner (url)))
  ([owner url]
   (kinsert models/alerters
            (k/values {:owner-id (:id owner)
                       :title (stringl 255)
                       :url-id (:id url)}))))

(defn shared-uri
  ([] (shared-uri (user)))
  ([owner] (shared-uri owner (string)))
  ([owner uri] (shared-uri owner uri (str (UUID/randomUUID))))
  ([owner uri access-token]
   (kinsert models/shared-uris
            (k/values
             {:owner-id (:id owner)
              :uri uri
              :access-token
              (UUID/fromString access-token)}))))
