(ns status-check.test.fixtures
  (:require [luminus-migrations.core :as migrations]
            [mount.core :as mount])
  (:require [status-check.config :refer [env]]
            [status-check.db.core]
            [status-check.handler]))

(defn app [f]
  (mount/start
   #'status-check.config/env
   #'status-check.db.core/*db*
   #'status-check.handler/app)
  (migrations/migrate ["migrate"] (select-keys env [:database-url]))
  (f))
