(ns status-check.test.checks.core
  (:require [clojure.test :refer [use-fixtures testing is]])
  (:require [korma.core :as k])
  (:require [status-check.db.models :as models]
            [status-check.checks.core :as core]
            [status-check.test.core :refer [defdbtest]]
            [status-check.test.fixtures :as fixtures]
            [status-check.test.generators :as g]
            [status-check.utils :as utils]))

(use-fixtures :once fixtures/app)

(defdbtest test-owner?
  (let [user (g/user)
        check0 (g/check)
        check1 (g/check user)]
    (is (not (core/owner? (:id user) (:id check0))))
    (is (core/owner? (:id user) (:id check1)))))

(defdbtest test-update!
  (with-redefs-fn {#'status-check.checks.scheduler/schedule! (fn [_&])
                   #'status-check.checks.scheduler/unschedule! (fn [_&])}
    ;; TODO Check redefed fns
    #(do (testing "Just updating"
           (let [check (g/check)
                 new-active? (g/boolean)
                 new-title (g/string)
                 new-check-interval (rand-int 30)
                 new-http-method (g/http-method)
                 new-url-address (g/url-address)
                 _ (core/update! (:id check) new-active? new-title new-check-interval
                                 new-http-method new-url-address)
                 updated-check
                 (first (k/select models/checks (k/where {:id (:id check)})))
                 updated-url
                 (first (k/select models/urls
                                  (k/where {:id (:url-id check)})))]
             (is (= (:is-active updated-check) new-active?))
             (is (= (:title updated-check) new-title))
             (is (= (utils/interval->seconds (:check-interval updated-check))
                    new-check-interval))
             (is (= (:http-method updated-url) new-http-method))
             (is (= (:url updated-url) new-url-address))))
         (testing "Create url and update"
           (let [check (g/check (g/user) nil)
                 new-active? (g/boolean)
                 new-title (g/string)
                 new-check-interval (rand-int 30)
                 new-http-method (g/http-method)
                 new-url-address (g/url-address)
                 _ (core/update! (:id check) new-active? new-title new-check-interval
                                 new-http-method new-url-address)
                 updated-check
                 (first (k/select models/checks (k/where {:id (:id check)})))
                 created-url
                 (first (k/select
                         models/urls
                         (k/where {:id (:url-id updated-check)})))]
             (is (= (:is-active updated-check) new-active?))
             (is (= (:title updated-check) new-title))
             (is (= (utils/interval->seconds (:check-interval updated-check))
                    new-check-interval))
             (is (= (:http-method created-url) new-http-method))
             (is (= (:url created-url) new-url-address)))))))

(defdbtest test-create!
  (let [owner (g/user)
        title (g/string)
        check (core/create! (:id owner) title)]
    (is (= (:owner-id check) (:id owner)))
    (is (= (:title check) title))))
