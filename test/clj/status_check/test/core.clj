(ns status-check.test.core
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.test :refer [deftest is]])
  (:require [buddy.auth]
            [conman.core :refer [with-transaction]]
            [korma.core :as k]
            [korma.db])
  (:require [status-check.db.core :as db]
            [status-check.db.models :as models]
            [status-check.db.utils :refer [kinsert]]
            [status-check.test-middleware]
            [status-check.test.generators :as g]))

(defmacro with-rollback [& body]
  `(with-transaction [db/*db*]
     (binding [korma.db/*current-conn* db/*db*]
       (jdbc/db-set-rollback-only! db/*db*)
       ~@body)))

(defmacro defdbtest [name & body]
  `(deftest ~name (with-rollback ~@body)))

(defn with-identity [id cb]
  (binding [status-check.test-middleware/*identity* id]
    (cb)))

(defn- count-users []
  (-> (db/count-all-users) first :count))

(defn- is-users-count [n]
  (is (= n (count-users) n)))

(deftest test-hugsql-rollback
  (let [n-users (count-users)]
    (with-rollback
      (db/create-user<! {:email (g/email) :password (g/password)})
      (is-users-count (+ 1 n-users)))
    (is-users-count n-users)))

(deftest test-korma-rollback
  (let [n-users (count-users)]
    (with-rollback
      (kinsert models/users (k/values {:email (g/email) :password (g/password)}))
      (is-users-count (+ 1 n-users)))
    (is-users-count n-users)))
