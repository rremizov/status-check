(ns status-check.test.sharing.core
  (:require [clojure.test :refer [use-fixtures testing is]])
  (:require [korma.core :as k])
  (:require [status-check.sharing.core :as core]
            [status-check.db.models :as models]
            [status-check.test.core :refer [defdbtest]]
            [status-check.test.fixtures :as fixtures]
            [status-check.test.generators :as g]
            [status-check.utils :as utils])
  (:import [java.util UUID]))

(use-fixtures :once fixtures/app)

(defdbtest test-shared-uris->strings
  (let [owner0 (g/user)
        owner1 (g/user)]
    (dotimes [_ 5] (g/shared-uri owner0))
    (dotimes [_ 5] (g/shared-uri owner1))
    (is (= 5 (count (core/shared-uris->strings (:id owner0)))))
    (is (= 5 (count (core/shared-uris->strings (:id owner1)))))))

(defdbtest test-share-uri!
  (testing "Insert"
    (is (= 0 (count (k/select models/shared-uris))))
    (dotimes [_ 3] (core/share-uri! (:id (g/user)) (g/string)))
    (is (= 3 (count (k/select models/shared-uris)))))
  (testing "Access token unique constraint"
    (with-redefs-fn
      {#'status-check.sharing.core/make-access-token
       (fn [] (UUID/fromString "0b259c20-b818-439f-8178-c5c08cd2ec1c"))}
      #(do (core/share-uri! (:id (g/user)) (g/string))
           (is (thrown? org.postgresql.util.PSQLException
                        (core/share-uri! (g/user) (g/string))))))))

(defdbtest test-delete-shared-uri!
  (is (= 0 (count (k/select models/shared-uris))))
  (let [uri (g/shared-uri)]
    (is (= 1 (count (k/select models/shared-uris))))
    (core/delete-shared-uri! (:id uri)))
  (is (= 0 (count (k/select models/shared-uris)))))

(defdbtest test-owner?
  (let [owner (g/user)
        not-owner (g/user)
        uri (core/share-uri! (:id owner) (g/string))]
    (is (core/owner? (:id owner) (:id uri)))
    (is (not (core/owner? (:id not-owner) (:id uri))))))

(defdbtest test-authorized?
  (with-redefs-fn
    {#'status-check.sharing.core/make-access-token
     (fn [] (UUID/fromString "0b259c20-b818-439f-8178-c5c08cd2ec1c"))}
    #(let [owner (g/user)
           not-owner (g/user)
           shared-uri (core/share-uri! (:id owner) (g/string))]
       (is (core/authorized? (:id owner) (:uri shared-uri)
                             "0b259c20-b818-439f-8178-c5c08cd2ec1c"))
       (is (not (core/authorized? (rand-int 1000000) (:uri shared-uri)
                                  "0b259c20-b818-439f-8178-c5c08cd2ec1c")))
       (is (not (core/authorized? (:id owner) (g/string)
                                  "0b259c20-b818-439f-8178-c5c08cd2ec1c")))
       (is (not (core/authorized? (:id owner) (:uri shared-uri)
                                  "0b259c20-b818-439f-8178-c5c08cd2ec1"))))))
