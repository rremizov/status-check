(ns status-check.test.db.utils
  (:require [clojure.data.generators :as g]
            [clojure.test :refer [deftest is]])
  (:require [status-check.db.utils :as utils])
  (:import [java.sql Date]))

(deftest test-sqldate->javadate
  (let [time 1190468701663
        sqldate (java.sql.Date. time)
        javadate (utils/sqldate->javadate sqldate)]
    (is (= time (.getTime javadate)))))

(deftest test-str->pgobject
  (let [type "json"
        value "{\"a\": 1, \"b\": [0, 2, 3]}"
        pgobject (utils/str->pgobject type value)]
    (is (= type (.getType pgobject)))
    (is (= value (.getValue pgobject)))))

(deftest test->pgjson
  (let [value {:a 1 :b [0 2 3]}
        pgjson (utils/->pgjson value)]
    (is (= "json" (.getType pgjson)))
    (is (= "{\"a\":1,\"b\":[0,2,3]}" (.getValue pgjson)))))

(deftest test->pgjsonb
  (let [value {:a 1 :b [0 2 3]}
        pgjsonb (utils/->pgjsonb value)]
    (is (= "jsonb" (.getType pgjsonb)))
    (is (= "{\"a\":1,\"b\":[0,2,3]}" (.getValue pgjsonb)))))

(deftest test-map->pginterval
  (let [interval {:seconds 5.0 :minutes 4 :hours 3 :days 10 :months 2 :years 1}
        pginterval (utils/map->pginterval interval)]
    (is (= (:seconds interval) (.getSeconds pginterval)))
    (is (= (:minutes interval) (.getMinutes pginterval)))
    (is (= (:hours interval)   (.getHours pginterval)))
    (is (= (:days interval)    (.getDays pginterval)))
    (is (= (:months interval)  (.getMonths pginterval)))
    (is (= (:years interval)   (.getYears pginterval)))))

