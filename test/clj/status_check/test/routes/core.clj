(ns status-check.test.routes.core
  (:require [clojure.test :refer [use-fixtures testing is]])
  (:require [net.cgrand.enlive-html :as html]
            [peridot.core :as p]
            [ring.mock.request :as r])
  (:require [status-check.handler :refer [app]]
            [status-check.test.core :refer [defdbtest with-identity]]
            [status-check.test.fixtures :as fixtures]
            [status-check.test.generators :as g]))

(use-fixtures :once fixtures/app)

(defn redirect? [response regex]
  (not (nil? (re-seq regex (-> response :headers (get "Location" ""))))))

(defn- parse-csrf-token
  "Given an HTML response, parse the body for an anti-forgery input field"
  [response]
  (-> (html/select (html/html-snippet (:body response))
                   [:input#__anti-forgery-token])
      first
      (get-in [:attrs :value])))

(defmacro csrf-token->>request [r params & body]
  `(let [r# ~r]
     (p/request r# ~@body
                :params (merge ~params
                               {"__anti-forgery-token"
                                (or (parse-csrf-token (:response r#))
                                    "")}))))

(defn get-page
  ([uri] (get-page uri {}))
  ([uri params]
   (-> (p/session app)
       (p/request uri :request-method :get :params params)
       :response)))

(defn get-page-as
  ([uri auth-as] (get-page-as uri auth-as {}))
  ([uri auth-as params]
   (assert (clojure.string/ends-with? uri "/"))
   (with-identity (:id auth-as)
     #(app (r/request :get uri params)))))

(defn post-form
  ([uri params]
   (post-form (p/session app) uri params))
  ([session uri params]
   (post-form session uri {} uri params))
  ([session get-uri get-params post-uri post-params]
   (assert (clojure.string/ends-with? get-uri "/"))
   (assert (clojure.string/ends-with? post-uri "/"))
   (-> session
       (p/request get-uri :request-method :get :params get-params)
       (csrf-token->>request post-params post-uri :request-method :post))))

(defn post-form-as
  ([user uri params] (post-form-as user uri {} uri params))
  ([user get-uri get-params post-uri post-params]
   (with-identity (:id user)
     #(post-form (p/session app) get-uri get-params post-uri post-params))))

(defn post-uri [uri params]
  (app (r/request :post uri params)))

(defdbtest test-routes
  (testing "/"
    (is (= 200 (-> (get-page "/") :status)))
    (is (= 302 (-> (get-page-as "/" (g/user)) :status)))))
