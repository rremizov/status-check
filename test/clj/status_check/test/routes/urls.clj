(ns status-check.test.routes.urls
  (:require [clojure.test :refer [use-fixtures testing is]])
  (:require [peridot.core :as p]
            [ring.mock.request :as r])
  (:require [status-check.handler :refer [app]]
            [status-check.routes.urls :as urls]
            [status-check.test.core :refer [defdbtest with-identity]]
            [status-check.test.fixtures :as fixtures]
            [status-check.test.generators :as g]
            [status-check.test.routes.core :refer [redirect? get-page get-page-as post-uri post-form-as]]))

(use-fixtures :once fixtures/app)

(defdbtest test-page-existing-parameter
  (let [owner (g/user)
        url (g/url owner)
        url-parameter (g/url-parameter url)
        uri (str "/urls/" (:id url)
                 "/parameters/" (:id url-parameter) "/")]
    (testing "GET /urls/1/parameters/1/ access rules"
      (is (= 200 (:status (get-page-as uri owner))))
      (is (= 302 (:status (get-page uri)))))))

(defdbtest test-page-new-parameter
  (let [owner (g/user)
        url (g/url owner)
        uri (str "/urls/" (:id url) "/parameters/new/")]
    (testing "GET /urls/1/parameters/new/ access rules"
      (is (= 200 (:status (get-page-as uri owner))))
      (is (= 302 (:status (get-page uri)))))))

(defdbtest test-handler-update-parameter
  (let [owner (g/user)
        url (g/url owner)
        url-parameter (g/url-parameter url)
        uri (str "/urls/" (:id url)
                 "/parameters/" (:id url-parameter) "/")]
    (testing "POST /urls/1/parameters/1/ 403 FORBIDDEN"
      (is (= 403 (:status (post-uri uri {:name (g/string) :value (g/string)})))))
    (testing "POST /urls/1/parameters/1/ validation#0"
      (let [result (post-form-as owner uri {:name (g/string) :value (g/string)})
            response (:response result)]
        (is (= 302 (:status response)))
        (is  (redirect? response #"checks\/\d+\/$"))))
    (testing "POST /urls/1/parameters/1/ validation#1"
      (let [result (post-form-as owner uri {:name (g/string)})
            response (:response result)]
        (is (= 302 (:status response)))
        (is (not (nil? (get (-> response :flash :errors) :value))))))
    (testing "POST /urls/1/parameters/1/ validation#2"
      (let [result (post-form-as owner uri {:value (g/stringl 256)})
            response (:response result)]
        (is (= 302 (:status response)))
        (is (not (nil? (get (-> response :flash :errors) :name))))))
    (testing "POST /urls/1/parameters/1/ validation#3"
      (let [result (post-form-as owner uri
                                 {:name (g/stringl 256) :value (g/string)})
            response (:response result)]
        (is (= 302 (:status response)))
        (is (not (nil? (get (-> response :flash :errors) :name))))))
    (testing "POST /urls/1/parameters/1/ validation#4"
      (let [result (post-form-as owner uri
                                 {:name (g/string) :value (g/stringl 256)})
            response (:response result)]
        (is (= 302 (:status response)))
        (is (not (nil? (get (-> response :flash :errors) :value))))))))

(defdbtest test-handler-create-parameter
  (let [owner (g/user)
        url (g/url owner)
        uri (str "/urls/" (:id url) "/parameters/new/")]
    (testing "POST /urls/1/parameters/new/"
      (let [result (post-form-as owner uri {:name (g/string) :value (g/string)})
            response (:response result)]
        (is (= 302 (:status response)))
        (is  (redirect? response #"checks\/\d+\/$"))))
    (testing "POST /urls/1/parameters/new/ 403 FORBIDDEN"
      (is (= 403 (:status (post-uri uri {:name (g/string) :value (g/string)})))))))

