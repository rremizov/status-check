(ns status-check.test.routes.share
  (:require [clojure.test :refer [use-fixtures testing is]])
  (:require [status-check.test.core :refer [defdbtest]]
            [status-check.test.fixtures :as fixtures]
            [status-check.test.generators :as g]
            [status-check.test.routes.core :refer [redirect? post-form post-form-as post-uri]]))

(use-fixtures :once fixtures/app)

(defdbtest test-access-rules
  (let [owner (g/user)]
    (testing "/share/uri/ authorized"
      (is (redirect? (:response
                      (post-form-as owner
                                    "/checks/" {}
                                    "/share/uri/" {:uri "/checks/"}))
                     #"\/auth/profile\/")))
    (testing "/share/uri/ not authorized"
      (is (= 403 (:status (post-uri "/share/uri/" {:uri "/checks/"})))))
    (let [shared-uri (g/shared-uri owner "/checks/")]
      (testing "/share/uri/1/delete/ authorized"
        (is
         (redirect?
          (:response
           (post-form-as owner
                         "/checks/" {}
                         (str "/share/uri/" (:id shared-uri) "/delete/")
                         {:uri "/checks/"}))
          #"\/auth\/profile\/")))
      (testing "/share/uri/1/delete/ not authorized"
        (is (= 403 (:status (post-uri (str "/share/uri/"
                                           (:id shared-uri)
                                           "/delete/")
                                      {}))))))))
