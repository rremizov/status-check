(ns status-check.test.routes.summary
  (:require [clojure.test :refer [use-fixtures testing is]])
  (:require [status-check.test.core :refer [defdbtest]]
            [status-check.test.fixtures :as fixtures]
            [status-check.test.generators :as g]
            [status-check.test.routes.core :refer [get-page get-page-as]]))

(use-fixtures :once fixtures/app)

(defdbtest test-access-rules
  (let [owner (g/user)
        uri (str "/summary/")]
    (testing "/summary/"
      (is (= 302 (:status (get-page uri))))
      (is (= 200 (:status (get-page-as uri owner)))))))
