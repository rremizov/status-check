(ns status-check.test.routes.incidents
  (:require [clojure.test :refer [use-fixtures testing is]])
  (:require [peridot.core :as p]
            [ring.mock.request :as r])
  (:require [status-check.handler :refer [app]]
            [status-check.sharing.core :as sharing]
            [status-check.test.core :refer [defdbtest]]
            [status-check.test.fixtures :as fixtures]
            [status-check.test.generators :as g]
            [status-check.test.routes.core :refer [get-page get-page-as]]))

(use-fixtures :once fixtures/app)

(defdbtest test-access-rules
  (let [owner (g/user)]
    (testing "/incidents/"
      (is (= 302 (:status (get-page "/incidents/"))))
      (is (= 200 (:status (get-page-as "/incidents/" owner)))))
    (testing "/incidents/ sharing"
      (is (= 302 (:status (get-page "/incidents/"))))
      (is (= 200 (:status (get-page-as "/incidents/" owner))))
      (let [shared-uri (g/shared-uri owner "/incidents/")]
        (is (= 200 (:status (get-page (sharing/uri->href shared-uri)))))))))
