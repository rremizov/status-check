(ns status-check.test.routes.auth
  (:require [clojure.test :refer [use-fixtures testing is]]
            [clojure.tools.logging :as log])
  (:require [peridot.core :as p])
  (:require [status-check.config :refer [env]]
            [status-check.handler :refer [app]]
            [status-check.test.core :refer [defdbtest]]
            [status-check.test.fixtures :as fixtures]
            [status-check.test.generators :as g]
            [status-check.test.routes.core :refer [get-page get-page-as post-form]]))

(use-fixtures :once fixtures/app)

(defdbtest test-routes
  (testing "/auth/signup/"
    (let [response
          (-> (p/session app)
              (post-form "/auth/signup/"
                         {:access-token (:signup-access-token env)}
                         "/auth/signup/"
                         {:email (g/email) :password (g/string)
                          :access-token (:signup-access-token env)})
              (p/request "/auth/profile/" :request-method :get)
              :response)]
      (is (= 200 (:status response)))))
  (testing "/auth/login/"
    (let [email (g/email)
          password (g/password)
          _ (g/user email password)
          response
          (-> (post-form "/auth/login/" {:email email :password password})
              (p/request "/auth/profile/" :request-method :get)
              :response)]
      (is (= 200 (:status response)))))
  (testing "/auth/logout/"
    (let [email (g/email)
          password (g/string)
          _ (g/user email password)
          response
          (-> (post-form "/auth/login/" {:email email :password password})
              (p/follow-redirect)
              (post-form "/auth/profile/" {} "/auth/logout/" {})
              :response)]
      (is (= 302 (:status response))))))

(defdbtest test-validation-signup
  (g/user "mail@test.net" "password")
  (letfn [(signup [params]
            (-> (p/session app)
                (post-form "/auth/signup/"
                           {:access-token (:signup-access-token env)}
                           "/auth/signup/"
                           (assoc params :access-token (:signup-access-token env)))
                :response))]
    (testing "required"
      (is (not (nil? (-> (signup {:password (g/string)})
                         :flash :errors :email))))
      (is (not (nil? (-> (signup {:email (g/email)})
                         :flash :errors :password)))))
    (testing "email format"
      (is (not (nil? (-> (signup {:email (g/string) :password (g/string)})
                         :flash :errors :email)))))
    (testing "is email free?"
      (is (not (nil? (-> (signup {:email "mail@test.net" :password (g/string)})
                         :flash :errors :email)))))
    (testing "password length"
      (is (not (nil? (-> (signup {:email "mail@test.net" :password (g/stringl 5)})
                         :flash :errors :password)))))))

(defdbtest test-validation-login
  (g/user "mail@test.net" "password")
  (letfn [(login [params] (:response (post-form "/auth/login/" params)))]
    (testing "required"
      (is (not (nil? (-> (login {:password (g/string)})
                         :flash :errors :email))))
      (is (not (nil? (-> (login {:email (g/email)})
                         :flash :errors :password)))))
    (testing "does user exist?"
      (is (not (nil? (-> (login {:email (g/email) :password (g/string)})
                         :flash :errors :email))))
      (is (nil? (-> (login {:email "mail@test.net" :password (g/string)})
                    :flash :errors :email))))
    (testing "is password correct?"
      (is (not (nil? (-> (login {:email "mail@test.net" :password (g/string)})
                         :flash :errors :password))))
      (is (nil? (-> (login {:email "mail@test.net" :password "password"})
                    :flash :errors :password))))))

(defdbtest test-access-rules
  (testing "/auth/profile/"
    (is (= 302 (:status (get-page "/auth/profile/"))))
    (is (= 200 (:status (get-page-as "/auth/profile/" (g/user))))))
  (testing "/auth/login/"
    (is (= 200 (:status (get-page "/auth/login/"))))
    (is (= 302 (:status (get-page-as "/auth/login/" (g/user))))))
  (testing "/auth/signup/"
    (is (= 200 (:status (get-page "/auth/signup/"
                                  {:access-token (:signup-access-token env)}))))
    (is (= 302 (:status (get-page-as "/auth/signup/" (g/user)))))))
