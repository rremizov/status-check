(ns status-check.test.routes.checks
  (:require [clojure.test :refer [use-fixtures testing is]])
  (:require [peridot.core :as p]
            [ring.mock.request :as r])
  (:require [status-check.handler :refer [app]]
            [status-check.routes.urls :as urls]
            [status-check.sharing.core :as sharing]
            [status-check.test.core :refer [defdbtest with-identity]]
            [status-check.test.fixtures :as fixtures]
            [status-check.test.generators :as g]
            [status-check.test.routes.core :refer [redirect? get-page get-page-as post-form post-form-as]]))

(use-fixtures :once fixtures/app)

(defdbtest test-forms
  (with-redefs-fn {#'status-check.checks.scheduler/schedule! (fn [_&])
                   #'status-check.checks.scheduler/unschedule! (fn [_&])}
    #(do (testing "/checks/1/"
           (let [owner (g/user)
                 check (g/check owner)
                 response
                 (:response
                  (post-form-as owner
                                (str "/checks/" (:id check) "/")
                                {:is-active (g/boolean)
                                 :title (g/string)
                                 :check-interval (rand-int 60)
                                 :http-method (g/http-method)
                                 :url (g/url)}))]
             (is (= 302 (:status response)))
             (is (redirect? response #"checks\/\d+\/$"))))
         (testing "/checks/new/"
           (let [user (g/user)
                 response
                 (:response
                  (post-form-as user "/checks/new/" {:title (g/string)}))]
             (is (= 302 (:status response)))
             (is (redirect? response #"checks\/\d+$")))))))

(defdbtest test-validation-new-check
  (letfn [(new-check [params]
            (:response (post-form-as (g/user) "/checks/new/" params)))]
    (testing "title required"
      (is (not (nil? (-> (new-check {}) :flash :errors :title))))
      (is (nil? (-> (new-check {:title (g/string)}) :flash :errors :title))))
    (testing "title max length"
      (is (not (nil? (-> (new-check {:title (g/stringl 256)})
                         :flash :errors :title)))))))

(defdbtest test-validation-update-check
  (letfn [(update-check [params]
            (let [owner (g/user)
                  check (g/check owner)]
              (:response
               (post-form-as owner (str "/checks/" (:id check) "/") params))))]
    (testing "required"
      (is (not (nil? (-> (update-check {}) :flash :errors :title))))
      (is (not (nil? (-> (update-check {}) :flash :errors :check-interval))))
      (is (not (nil? (-> (update-check {}) :flash :errors :http-method))))
      (is (not (nil? (-> (update-check {}) :flash :errors :url))))
      (is (nil? (-> (update-check {:title (g/string)}) :flash :errors :title)))
      (is (nil? (-> (update-check {:check-interval (rand-int 10)})
                    :flash :errors :check-interval)))
      (is (nil? (-> (update-check {:http-method (g/http-method)})
                    :flash :errors :http-method)))
      (is (nil? (-> (update-check {:url (g/url)}) :flash :errors :url))))
    (testing ":title max length"
      (is (not (nil? (-> (update-check {:title (g/stringl 256)})
                         :flash :errors :title)))))
    (testing ":check-interval - integer?"
      (is (not (nil? (-> (update-check {:check-interval (g/string)})
                         :flash :errors :check-interval)))))
    (testing ":http-method"
      (is (not (nil? (-> (update-check {:http-method (g/string)})
                         :flash :errors :http-method)))))
    (testing ":url max length"
      (is (not (nil? (-> (update-check {:url (g/stringl 256)})
                         :flash :errors :url)))))))

(defdbtest test-access-rules
  (let [owner (g/user)
        check (g/check owner)]
    (testing "/checks/"
      (is (= 302 (:status (get-page "/checks/"))))
      (is (= 200 (:status (get-page-as "/checks/" owner)))))
    (testing "/checks/ sharing"
      (is (= 302 (:status (get-page "/checks/"))))
      (is (= 200 (:status (get-page-as "/checks/" owner))))
      (let [shared-uri (g/shared-uri owner "/checks/")]
        (is (= 200 (:status (get-page (sharing/uri->href shared-uri)))))))
    (testing "/checks/new/"
      (is (= 302 (:status (get-page "/checks/new/"))))
      (is (= 200 (:status (get-page-as "/checks/new/" owner)))))

    (let [uri (str "/checks/" (:id check) "/")]
      (testing "/checks/1/"
        (is (= 302 (:status (get-page uri))))
        (is (= 200 (:status (get-page-as uri owner))))))))
