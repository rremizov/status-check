(ns status-check.test.routes.alerters
  (:require [clojure.test :refer [use-fixtures testing is]])
  (:require [peridot.core :as p]
            [ring.mock.request :as r])
  (:require [status-check.handler :refer [app]]
            [status-check.routes.urls :as urls]
            [status-check.test.core :refer [defdbtest with-identity]]
            [status-check.test.fixtures :as fixtures]
            [status-check.test.generators :as g]
            [status-check.test.routes.core :refer [redirect? get-page get-page-as post-form post-form-as]]))

(use-fixtures :once fixtures/app)

(defdbtest test-routes
  (testing "/alerters/1/"
    (let [owner (g/user)
          alerter (g/alerter owner)
          response
          (:response
           (post-form-as owner
                         (str "/alerters/" (:id alerter) "/")
                         {:is-active (g/boolean)
                          :title (g/string)
                          :type (g/alerter-type-name)
                          :http-method (g/http-method)
                          :url (g/url)}))]
      (is (= 302 (:status response)))
      (is (redirect? response #"alerters\/\d+\/$"))))
  (testing "/alerters/new/"
    (let [user (g/user)
          response
          (:response (post-form-as user "/alerters/new/" {:title (g/string)}))]
      (is (= 302 (:status response)))
      (is (redirect? response #"alerters\/\d+$")))))

(defdbtest test-validation-new-alerter
  (letfn [(new-alerter [params]
            (:response (post-form-as (g/user) "/alerters/new/" params)))]
    (testing "title required"
      (is (not (nil? (-> (new-alerter {}) :flash :errors :title))))
      (is (nil? (-> (new-alerter {:title (g/string)}) :flash :errors :title))))
    (testing "title max length"
      (is (not (nil? (-> (new-alerter {:title (g/stringl 256)})
                         :flash :errors :title)))))))

(defdbtest test-validation-update-alerter
  (letfn [(update-alerter [params]
            (let [owner (g/user)
                  alerter (g/alerter owner)]
              (:response
               (post-form-as owner (str "/alerters/" (:id alerter) "/") params))))]
    (testing "required"
      (is (not (nil? (-> (update-alerter {}) :flash :errors :title))))
      (is (not (nil? (-> (update-alerter {}) :flash :errors :type))))
      (is (not (nil? (-> (update-alerter {}) :flash :errors :http-method))))
      (is (not (nil? (-> (update-alerter {}) :flash :errors :url))))
      (is (nil? (-> (update-alerter {:title (g/string)}) :flash :errors :title)))
      (is (nil? (-> (update-alerter {:type (g/alerter-type-name)})
                    :flash :errors :type)))
      (is (nil? (-> (update-alerter {:http-method (g/http-method)})
                    :flash :errors :http-method)))
      (is (nil? (-> (update-alerter {:url (g/url)}) :flash :errors :url))))
    (testing ":title max length"
      (is (not (nil? (-> (update-alerter {:title (g/stringl 256)})
                         :flash :errors :title)))))
    (testing ":type - valid?"
      (is (not (nil? (-> (update-alerter {:type (g/string)})
                         :flash :errors :type)))))
    (testing ":http-method"
      (is (not (nil? (-> (update-alerter {:http-method (g/string)})
                         :flash :errors :http-method)))))
    (testing ":url max length"
      (is (not (nil? (-> (update-alerter {:url (g/stringl 256)})
                         :flash :errors :url)))))))

(defdbtest test-access-rules
  (let [owner (g/user)
        alerter (g/alerter owner)]
    (testing "/alerters/"
      (is (= 302 (:status (get-page "/alerters/"))))
      (is (= 200 (:status (get-page-as "/alerters/" owner)))))
    (testing "/alerters/new/"
      (is (= 302 (:status (get-page "/alerters/new/"))))
      (is (= 200 (:status (get-page-as "/alerters/new/" owner)))))

    (let [uri (str "/alerters/" (:id alerter) "/")]
      (testing "/alerters/1/"
        (is (= 302 (:status (get-page uri))))
        (is (= 200 (:status (get-page-as uri owner))))))))

