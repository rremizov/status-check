(ns status-check.test.urls.core
  (:require [clojure.test :refer [use-fixtures is]])
  (:require [korma.core :as k])
  (:require [status-check.urls.core :as core]
            [status-check.db.models :as models]
            [status-check.test.core :refer [defdbtest]]
            [status-check.test.fixtures :as fixtures]
            [status-check.test.generators :as g]))

(use-fixtures :once fixtures/app)

(defdbtest test-owner?
  (let [user (g/user)
        url (g/url)
        check (g/check user url)]
    (is (core/owner? (:id user) (:id url)))
    (is (not (core/owner? (:id (g/user)) (:id url))))))

(defdbtest test-update-parameter!
  (let [initial (g/url-parameter)
        new-name (g/string)
        new-value (g/string)
        _ (core/update-parameter! (:id initial) new-name new-value)
        updated (first (k/select models/url-parameters
                                 (k/where {:id (:id initial)})))]
    (is (= (:name updated) new-name))
    (is (= (:value updated) new-value))))

(defdbtest test-create-parameter!
  (let [url (g/url)
        pname (g/string)
        value (g/string)
        new (core/create-parameter! (:id url) pname value)]
    (is (= (:url-id new) (:id url)))
    (is (= (:name new) pname))
    (is (= (:value new) value))))

(defdbtest test-last-response
  (let [url (g/url)
        last-response
        (->> (partial g/url-response url)
             (repeatedly)
             (take 3)
             (last))]
    (is (= (:id (core/last-response (:id url)))
           (:id last-response)))))

(defdbtest test-make-request
  (let [url (g/url)]
    (with-redefs-fn {#'clj-http.client/request g/http-response}
      #(let [response (core/make-request! url)]
         (is (and (contains? response :url-id)
                  (contains? response :status)
                  (contains? response :body)
                  (contains? response :request-time)
                  (contains? response :created-at)
                  (contains? response :headers)))
         (is (= (:url-id response) (:id url)))))))

