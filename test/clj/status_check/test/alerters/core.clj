(ns status-check.test.alerters.core
  (:require [clojure.test :refer [use-fixtures testing is]])
  (:require [korma.core :as k])
  (:require [status-check.alerters.core :as core]
            [status-check.db.models :as models]
            [status-check.test.core :refer [defdbtest]]
            [status-check.test.fixtures :as fixtures]
            [status-check.test.generators :as g]
            [status-check.utils :as utils]))

(use-fixtures :once fixtures/app)

(defdbtest test-owner?
  (let [user (g/user)
        alerter0 (g/alerter)
        alerter1 (g/alerter user)]
    (is (not (core/owner? (:id user) (:id alerter0))))
    (is (core/owner? (:id user) (:id alerter1)))))

(defdbtest test-update!
  (testing "Just update"
    (let [alerter (g/alerter)
          new-active? (g/boolean)
          new-title (g/string)
          new-http-method (g/http-method)
          [new-type-id new-type-name] (g/alerter-type)
          new-url-address (g/url-address)
          _ (core/update! (:id alerter) new-active? new-title new-type-name
                          new-http-method new-url-address 0)
          updated-alerter
          (first (k/select models/alerters (k/where {:id (:id alerter)})))
          updated-url
          (first (k/select models/urls
                           (k/where {:id (:url-id alerter)})))]
      (is (= (:is-active updated-alerter) new-active?))
      (is (= (:title updated-alerter) new-title))
      (is (= (:type-id updated-alerter) new-type-id))
      (is (= (:http-method updated-url) new-http-method))
      (is (= (:url updated-url) new-url-address))))
  (testing "Create url and update"
    (let [alerter (g/alerter (g/user) nil)
          new-active? (g/boolean)
          new-title (g/string)
          [new-type-id new-type-name] (g/alerter-type)
          new-http-method (g/http-method)
          new-url-address (g/url-address)
          _ (core/update! (:id alerter) new-active? new-title new-type-name
                          new-http-method new-url-address 0)
          updated-alerter
          (first (k/select models/alerters (k/where {:id (:id alerter)})))
          created-url
          (first (k/select
                  models/urls
                  (k/where {:id (:url-id updated-alerter)})))]
      (is (= (:is-active updated-alerter) new-active?))
      (is (= (:title updated-alerter) new-title))
      (is (= (:type-id updated-alerter) new-type-id))
      (is (= (:http-method created-url) new-http-method))
      (is (= (:url created-url) new-url-address)))))

(defdbtest test-create!
  (let [owner (g/user)
        title (g/string)
        alerter (core/create! (:id owner) title)]
    (is (= (:owner-id alerter) (:id owner)))
    (is (= (:title alerter) title))))

