FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/uberjar/status-check.jar /status-check/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/status-check/app.jar"]
