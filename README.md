# Status Check

*"Ping an URL" utility with Web UI.*

Don’t have time or resources to deploy a full-blown monitoring solution?
Don’t want to pay for a third-party service to just “curl” your site once in a while?
Status Check has your back! Just push the “Deploy to Heroku” button!

_Built with [Luminus] v. 2.9.10.70_

## Features

- Define Checks to periodically ping URLs.
- Define Alerters to send notifications to Slack.

## Deploying

TBD

## Contributing

### Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

### Running

To start a web server for the application, run:

    lein run

### Building

#### Uberjar

    lein with-profile prod uberjar

#### Tomcat

    JAVA_TOOL_OPTIONS=-Dcatalina.base=./target lein with-profile prod/tomcat uberwar

## License

Distributed under the [EPL v1.0]
Copyright © 2016 Roman Remizov

[EPL v1.0]: http://www.eclipse.org/legal/epl-v10.html
[Luminus]: http://www.luminusweb.net/
