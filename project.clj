(defproject status-check "0.1.0-SNAPSHOT"

  :description "\"Ping an URL\" utility with Web UI."
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :url "https://github.com/rremizov/status-check"

  :dependencies [[bouncer "1.0.1"]
                 [buddy "0.13.0"]
                 [camel-snake-kebab "0.4.0"]
                 [cheshire "5.10.0"]
                 [clj-http "2.1.0"]
                 [clj-time "0.12.2"]
                 [clojurewerkz/quartzite "2.0.0"]
                 [compojure "1.6.2"]
                 [conman "0.5.8"]
                 [cprop "0.1.17"]
                 [enlive "1.1.6"]
                 [korma "0.4.3"]
                 [luminus-immutant "0.2.5"]
                 [luminus-log4j "0.1.3"]
                 [luminus-migrations "0.2.2"]
                 [luminus-nrepl "0.1.7"]
                 [markdown-clj "1.10.5"]
                 [metosin/ring-http-response "0.9.1"]
                 [mount "0.1.10"]
                 [org.clojure/clojure "1.9.0"]
                 [org.clojure/data.generators "0.1.2"]
                 [org.clojure/tools.cli "1.0.194"]
                 [org.clojure/tools.logging "1.1.0"]
                 [org.postgresql/postgresql "9.4-1206-jdbc4"]
                 [org.webjars.bower/tether "2.0.0-beta.5"]
                 [org.webjars/bootstrap "4.0.0-alpha.5"]
                 [org.webjars/font-awesome "4.6.3"]
                 [org.webjars/jquery "2.2.4"]
                 [org.webjars/webjars-locator-jboss-vfs "0.1.0"]
                 [peridot "0.4.4"]
                 [prismatic/plumbing "0.5.3"]
                 [ring-middleware-format "0.7.0"]
                 [ring-webjars "0.1.1"]
                 [ring/ring-defaults "0.3.2"]
                 [ring/ring-servlet "1.4.0"]
                 [selmer "1.12.31"]]

  :min-lein-version "2.0.0"

  :jvm-opts ["-server" "-Dconf=.lein-env"]
  :source-paths ["src/clj"]
  :resource-paths ["resources"]
  :target-path "target/%s/"
  :main status-check.core
  :migratus {:store :database :db ~(get (System/getenv) "DATABASE_URL")}

  :plugins [[lein-cprop "1.0.1"]
            [lein-immutant "2.1.0"]
            [lein-uberwar "0.2.0"]
            [migratus-lein "0.3.7"]]

  :uberwar
  {:handler status-check.handler/app
   :init status-check.handler/init
   :destroy status-check.handler/destroy
   :name "status-check.war"}

  :profile
  s
  {:uberjar {:omit-source true
             :aot :all
             :uberjar-name "status-check.jar"}

   :prod {:source-paths ["env/prod/clj"]
          :resource-paths ["env/prod/resources"]}

   :prod/tomcat [:prod {:resource-paths ^:replace ["env/tomcat/prod/resources"]}]

   :dev           [:project/dev :profiles/dev]
   :test          [:project/test :profiles/test]

   :project/dev  {:dependencies [[prone "2020-01-17"]
                                 [ring/ring-mock "0.4.0"]
                                 [ring/ring-devel "1.8.2"]
                                 [pjstadig/humane-test-output "0.10.0"]]
                  :plugins      [[com.jakemccrary/lein-test-refresh "0.14.0"]
                                 [lein-cljfmt "0.6.4"]]

                  :source-paths ["env/dev/clj" "test/clj"]
                  :resource-paths ["env/dev/resources"]
                  :repl-options {:init-ns user}
                  :injections [(require 'pjstadig.humane-test-output)
                               (pjstadig.humane-test-output/activate!)]}
   :project/test {:source-paths ["env/test/clj"]
                  :resource-paths ["env/dev/resources" "env/test/resources"]}
   :profiles/dev {}
   :profiles/test {}})
