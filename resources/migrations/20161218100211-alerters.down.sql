BEGIN;
DROP VIEW alerters_summary;
DROP INDEX checks_alerters_unique_ids;
DROP TABLE checks_alerters;
DROP TABLE alerter_alert;
DROP TABLE alerters;
DROP TABLE alerter_types;
COMMIT;
