BEGIN;

DROP AGGREGATE public.FIRST (anyelement);
DROP FUNCTION public.first_agg (anyelement, anyelement);

DROP AGGREGATE public.LAST (anyelement);
DROP FUNCTION public.last_agg (anyelement, anyelement);

COMMIT;
