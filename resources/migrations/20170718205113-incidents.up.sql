BEGIN;

CREATE TABLE incidents (
    id SERIAL PRIMARY KEY,
    url_id integer REFERENCES urls(id) NOT NULL,
    is_closed boolean DEFAULT false,
    created_at timestamp WITH time zone DEFAULT now(),
    updated_at timestamp WITH time zone DEFAULT now(),
    escalated_at timestamp WITH time zone DEFAULT now(),
    closed_at timestamp WITH time zone,

    CHECK (is_closed = false OR closed_at IS NOT NULL)
);

CREATE UNIQUE INDEX incidents_not_closed_unique
    ON incidents (url_id)
    WHERE is_closed = false;

CREATE VIEW incidents_summary AS (
    SELECT
        i.id,
        COALESCE(a.owner_id, p.owner_id) AS owner_id,
        ae.url,
        i.is_closed,
        i.created_at,
        i.updated_at
    FROM incidents AS i
    INNER JOIN urls AS ae ON (i.url_id = ae.id)
    LEFT OUTER JOIN checks AS p ON (p.url_id = i.url_id)
    LEFT OUTER JOIN alerters AS a ON (a.url_id = i.url_id)
    WHERE a.owner_id IS NOT NULL
    OR p.owner_id IS NOT NULL
    ORDER BY i.is_closed ASC, i.updated_at DESC
);

COMMIT;
