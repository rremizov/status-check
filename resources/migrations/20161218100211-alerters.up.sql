BEGIN;

CREATE TABLE alerter_types (
    id SERIAL PRIMARY KEY,
    name varchar(255) NOT NULL,
    created_at timestamp WITH time zone DEFAULT now()
);
INSERT INTO alerter_types (name) VALUES ('Slack');

CREATE TABLE alerters (
    id SERIAL PRIMARY KEY,
    owner_id integer REFERENCES users(id) NOT NULL,
    is_active boolean DEFAULT false,
    title varchar(255) NOT NULL,
    type_id integer REFERENCES alerter_types(id),
    url_id integer REFERENCES urls(id),
    escalation_point_seconds integer DEFAULT 0,
    created_at timestamp WITH time zone DEFAULT now()

    CHECK (escalation_point_seconds >= 0)
);

CREATE TABLE alerter_alert (
    id SERIAL PRIMARY KEY,
    alerter_id integer REFERENCES alerters(id) NOT NULL,
    payload jsonb DEFAULT '{}',
    created_at timestamp WITH time zone DEFAULT now()
);

CREATE TABLE checks_alerters (
    id SERIAL PRIMARY KEY,
    check_id integer REFERENCES checks(id) NOT NULL,
    alerter_id integer REFERENCES alerters(id) NOT NULL,
    created_at timestamp WITH time zone DEFAULT now()
);
CREATE UNIQUE INDEX checks_alerters_unique_ids
    ON checks_alerters (check_id, alerter_id);

CREATE VIEW alerters_summary AS (
    SELECT
        at.name AS type_name,
        a.*,
        aes.avg_request_time,
        aes.last_status,
        aes.last_request_at
    FROM urls_summary AS aes
    RIGHT OUTER JOIN alerters AS a ON (aes.url_id = a.url_id)
    LEFT OUTER JOIN alerter_types AS at ON (a.type_id = at.id)
);

COMMIT;
