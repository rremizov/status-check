BEGIN;
DROP TRIGGER checks_check_interval ON checks;
DROP FUNCTION checks_check_interval();
DROP INDEX checks_url_id_unique_idx;
DROP TABLE checks;
COMMIT;
