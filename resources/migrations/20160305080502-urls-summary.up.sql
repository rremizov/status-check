BEGIN;

CREATE VIEW urls_last_statuses AS (
    SELECT DISTINCT
        url_id,
        first_value(status) OVER w_ordered AS last_status
    FROM url_responses AS aer
    WINDOW
        w_ordered AS (PARTITION BY url_id ORDER BY created_at DESC)
);

CREATE VIEW urls_statistics AS (
    SELECT
        url_id,
        AVG(request_time) AS avg_request_time,
        MAX(created_at) AS last_request_at,
        percentile_cont(0.1)
            WITHIN GROUP (ORDER BY request_time)
            AS request_time_percentile_01,
        percentile_cont(0.5)
            WITHIN GROUP (ORDER BY request_time)
            AS request_time_percentile_05,
        percentile_cont(0.99)
            WITHIN GROUP (ORDER BY request_time)
            AS request_time_percentile_099
    FROM url_responses AS aer
    GROUP BY url_id
);

CREATE VIEW urls_summary AS (
    SELECT *
    FROM urls_statistics
    NATURAL INNER JOIN urls_last_statuses
);

COMMIT;
