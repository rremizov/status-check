CREATE VIEW checks_summary AS (
  SELECT
    p.*,
    aes.avg_request_time,
    aes.last_request_at,
    aes.request_time_percentile_01,
    aes.request_time_percentile_05,
    aes.request_time_percentile_099,
    aes.last_status
  FROM urls_summary AS aes
         RIGHT OUTER JOIN checks AS p ON (aes.url_id = p.url_id)
);
