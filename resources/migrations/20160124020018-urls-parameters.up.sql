CREATE TABLE url_parameters (
    id SERIAL PRIMARY KEY,
    url_id integer REFERENCES urls(id) NOT NULL,
    name varchar(255) NOT NULL,
    value varchar(255) DEFAULT '',
    UNIQUE (url_id, name)
);
