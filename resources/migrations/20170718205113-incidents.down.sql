BEGIN;

DROP VIEW incidents_summary;
DROP INDEX incidents_not_closed_unique;
DROP TABLE incidents;

COMMIT;
