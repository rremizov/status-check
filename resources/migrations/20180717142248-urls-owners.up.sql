-- TODO Use this view everywhere
CREATE VIEW urls_owners AS (
  SELECT
    u.id                             AS url_id,
    COALESCE(p.owner_id, a.owner_id) AS owner_id
  FROM urls AS u
         LEFT OUTER JOIN checks AS p ON u.id = p.url_id
         LEFT OUTER JOIN alerters AS a ON u.id = a.url_id
);
