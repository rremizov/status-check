BEGIN;
CREATE TABLE shared_uris (
    id serial PRIMARY KEY,
    owner_id integer REFERENCES users(id) NOT NULL,
    uri text NOT NULL,
    access_token uuid NOT NULL,
    created_at timestamp WITH TIME ZONE DEFAULT now()
);
CREATE UNIQUE INDEX shared_uris_unique_token_idx ON shared_uris (access_token);
CREATE UNIQUE INDEX shared_uris_unique_idx ON shared_uris (
    owner_id,
    uri,
    access_token
);
COMMIT;
