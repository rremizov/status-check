CREATE TABLE checks_statuses_changes (
    id SERIAL PRIMARY KEY,
    check_id integer REFERENCES checks(id) NOT NULL,
    from_id integer REFERENCES url_responses(id) ON DELETE CASCADE NOT NULL,
    to_id integer REFERENCES url_responses(id) ON DELETE CASCADE NOT NULL,
    created_at timestamp WITH time zone DEFAULT now()
);
