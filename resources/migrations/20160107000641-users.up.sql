BEGIN;
CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    email varchar(300),
    password varchar(300),
    last_login timestamp WITH time zone DEFAULT now(),
    is_active boolean DEFAULT true
);
CREATE UNIQUE INDEX users_email_unique_idx ON users (email);
COMMIT;
