CREATE TABLE url_responses (
    id serial PRIMARY KEY,
    url_id integer REFERENCES urls(id) NOT NULL,
    status integer CONSTRAINT positive_response_status CHECK (status > 0),
    headers jsonb DEFAULT '{}',
    body text DEFAULT '',
    request_time integer CONSTRAINT positive_request_status CHECK (request_time > 0),
    created_at timestamp WITH time zone DEFAULT now()
);
