BEGIN;

CREATE TABLE checks (
    id SERIAL PRIMARY KEY,
    is_active boolean DEFAULT false,
    owner_id integer REFERENCES users(id) NOT NULL,
    title varchar(255) NOT NULL,
    url_id integer REFERENCES urls(id),
    check_interval interval DEFAULT interval '1 minute'
);
CREATE UNIQUE INDEX checks_url_id_unique_idx
    ON checks (url_id);

CREATE FUNCTION checks_check_interval()
RETURNS trigger
AS $body$
    BEGIN
        NEW.check_interval := justify_interval(NEW.check_interval);
        RETURN NEW;
    END;
$body$
LANGUAGE plpgsql;

CREATE TRIGGER checks_check_interval
    BEFORE INSERT OR UPDATE ON checks
    FOR EACH ROW EXECUTE PROCEDURE checks_check_interval();

COMMIT;
