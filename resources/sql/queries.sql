-- :name count-all-users
-- :command :query
-- :result :many
-- :doc count all users
SELECT count(*) AS count FROM users;

-- :name create-user<!
-- :command :returning-execute
-- :result :one
-- :doc create-user
INSERT INTO users (email, password) VALUES (:email, :password) RETURNING *

-- :name save-url-response<!
-- :command :returning-execute
-- :result :one
-- :doc save http response
INSERT INTO url_responses
(url_id, status, headers, body, request_time)
VALUES
(:url-id, :status, :headers, :body, :request-time)
RETURNING *

-- :name url-responses-cleanup!
-- :command :execute
-- :result :affected
-- :doc delete old url responses
-- TODO pass created-at value dynamically
DELETE FROM url_responses
WHERE created_at < (NOW() AT TIME ZONE 'UTC') - INTERVAL '15 minutes'

-- :name incidents-cleanup!
-- :command :execute
-- :result :affected
-- :doc delete old closed incidents
DELETE FROM incidents
WHERE is_closed = true
AND closed_at < (NOW() AT TIME ZONE 'UTC') - INTERVAL '7 days'

-- :name incidents-pending-escalations
-- :command :query
-- :result :many
-- :doc get details of pending notifications
SELECT
    incidents.id AS incident_id,
    incident_url.url AS incident_url,
    alerters.id AS alerter_id
FROM incidents
INNER JOIN urls AS incident_url
    ON incidents.url_id = incident_url.id
INNER JOIN urls_owners AS url_owner
    ON incidents.url_id = url_owner.url_id
CROSS JOIN alerters
WHERE (
    incidents.is_closed = false
    AND alerters.is_active = true
    AND alerters.owner_id = url_owner.owner_id
    AND (
        EXTRACT(EPOCH FROM incidents.updated_at - incidents.created_at)
        >= alerters.escalation_point_seconds
    )
    AND (
        EXTRACT(EPOCH FROM incidents.escalated_at - incidents.created_at)
        <= alerters.escalation_point_seconds
    )
)

-- :name incidents-pending-closure-alerts
-- :command :query
-- :result :many
-- :doc get details of pending notifications
SELECT
    incidents.id AS incident_id,
    incident_url.url AS incident_url,
    alerters.id AS alerter_id
FROM incidents
INNER JOIN urls AS incident_url
    ON (incidents.url_id = incident_url.id)
INNER JOIN urls_owners AS url_owner
    ON incidents.url_id = url_owner.url_id
CROSS JOIN alerters
WHERE (
    incidents.is_closed = true
    AND alerters.is_active = true
    AND alerters.owner_id = url_owner.owner_id
    AND (
        EXTRACT(EPOCH FROM incidents.updated_at - incidents.created_at)
        >= alerters.escalation_point_seconds
    )
    AND incidents.escalated_at < incidents.closed_at
    AND incidents.escalated_at > incidents.created_at
)
